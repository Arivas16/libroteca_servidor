class GrupoLecturaUsuario < ActiveRecord::Base
	self.primary_keys = :grupo_lectura_id, :usuario_id
	belongs_to :tipo_amigo
	belongs_to :grupo_lectura
	belongs_to :usuario
end