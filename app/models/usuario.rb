class Usuario < ActiveRecord::Base

#mount_uploader :picture, PictureUploader #cargador de la foto

#************ASOCIACIONES***********

	has_many :usuario_amigo

	has_many :libro_usuario
	has_many :libro, through: :libro_usuario

	has_many :lista_deseos
	has_many :libro, through: :lista_deseos

	has_many :grupo_lectura_usuario
	has_many :grupo_lectura, through: :grupo_lectura_usuario
	has_many :comentario_grupo_lectura

	has_many :estado
	has_many :noticia
	has_many :notificacion
	has_many :solicitud , :foreign_key => "usuario_solicitante_id"

	belongs_to :tipo_configuracion





#************FUNCIONES**************
	def self.md5(clave) # función que permite encriptar las claves a MD5
		Digest::MD5.hexdigest("#{clave}")
	end

	def self.random_alphanumeric(size=8) # función generado de claves aleatorias
	    chars = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a #puede contener letras mayusculas y minusculas, ademas de los numeros
	    @clave=(0...size).collect { chars[Kernel.rand(chars.length)] }.join
	    return @clave
  	end

  	def self.amigos_id(usuario_id)
  		amigos_recibidos= UsuarioAmigo.select(:amigo_id).where(usuario_id:usuario_id, tipo_amigo_id: "ACEP")
    	amigos_enviados = UsuarioAmigo.select(:usuario_id).where(amigo_id:usuario_id, tipo_amigo_id: "ACEP")
    	@amigos = Array.new 
    	amigos_recibidos.each do |x|
    		@amigos.push(x.amigo_id)
    	end
    	amigos_enviados.each do |x|
    		@amigos.push(x.usuario_id)
    	end
    	return @amigos.uniq
  	end

  	
end
