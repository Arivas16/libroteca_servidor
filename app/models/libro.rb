class Libro < ActiveRecord::Base
	has_many :lista_deseos 
	has_many :usuario, through: :lista_deseos

	has_many :libro_usuario 
	has_many :usuario, through: :libro_usuario

	belongs_to :categoria

	has_many :autor
end