class FotoUsuario < ActiveRecord::Base
	belongs_to :usuario 

	has_attached_file :picture, styles: { thumb: "100x100",small:"200x200" }
	validates_attachment_size :picture, :less_than => 2.megabytes
	validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
end
