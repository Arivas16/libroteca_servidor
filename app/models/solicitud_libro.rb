class SolicitudLibro < ActiveRecord::Base
	belongs_to :usuario
	belongs_to :libro 
	belongs_to :tipo_solicitud_libro , :foreign_key => "amigo_id"
end
