class ListaDeseos < ActiveRecord::Base
	self.primary_keys = :libro_id, :usuario_id
	belongs_to :libro
	belongs_to :usuario
end