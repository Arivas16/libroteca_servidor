class GrupoLectura < ActiveRecord::Base
	has_many :grupo_lectura_usuario
	has_many :comentario_grupo_lectura
	belongs_to :usuario
end