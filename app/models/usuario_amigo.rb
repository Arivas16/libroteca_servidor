class UsuarioAmigo < ActiveRecord::Base
	self.primary_keys = :usuario_id, :amigo_id
	belongs_to :usuario , :foreign_key => "amigo_id"
	belongs_to :tipo_amigo
	
end