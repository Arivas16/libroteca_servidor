class LibroUsuario < ActiveRecord::Base
	self.primary_keys = :usuario_id, :libro_id
	belongs_to :usuario 
	belongs_to :libro
	belongs_to :tipo_modalidad
	belongs_to :tipo_condicion
	belongs_to :subcategoria
	belongs_to :categoria
end