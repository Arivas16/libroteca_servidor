class Noticia < ActiveRecord::Base
	
	belongs_to :tipo_noticia
	belongs_to :usuario
	belongs_to :estado
	belongs_to :grupo_lectura
	belongs_to :libro

end