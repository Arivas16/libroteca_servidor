class Notificacion < ActiveRecord::Base
	
	belongs_to :usuario, :foreign_key => "usuario_id"
	belongs_to :tipo_notificacion
	belongs_to :estado
	belongs_to :grupo_lectura
	belongs_to :libro
	
end