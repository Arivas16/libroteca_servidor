class Solicitud < ActiveRecord::Base
	self.primary_keys = :usuario_solicitante_id, :usuario_dueno_id, :libro_id
	belongs_to :estado_solicitud
	belongs_to :tipo_modalidad
	belongs_to :libro
	belongs_to :libro_usuario, foreign_key: [:usuario_dueno_id, :libro_id]
	belongs_to :usuario , :foreign_key => "usuario_solicitante_id"
end