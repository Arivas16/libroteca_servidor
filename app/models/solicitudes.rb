class Solicitudes < ActiveRecord::Base
	belongs_to :estado_solicitud
	belongs_to :tipo_modalidad
	belongs_to :libro
	belongs_to :usuario , :foreign_key => "usuario_solicitante_id"
end