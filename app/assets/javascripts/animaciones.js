

$(document).ready(function(){
    $('.active .carousel-caption#slide1').stop(true, true).delay(50).animate(
            {bottom: '176px'},"slow");

    $('#carousel-example-generic').on('slid.bs.carousel', function () {
        $(this).find('.active .carousel-caption#slide1').stop(true, true).delay(50).animate(
            {bottom: '176px'},"slow");
        $(this).find('.active .carousel-caption#slide2').stop(true, true).delay(50).animate(
            { top: "170px" }, "slow");
        $("#slide1").css("bottom","600px");
        $("#slide2").css("top","600px")
    });

     $('#carousel-example-generic').on('relatedTarget', function () {alert("holis");});

    
	///**************MODAL**********************	
	var cant_autor=0;
	$("#abrir-modal-contrasena").click(function(){
		$('#myModalIniciarSesion').modal("hide");
		$('#myModalOlvidarContrasena').modal("show");
	});


	///**************AGREGAR LIBRO**********************
	$("#agregar_aut").click(function() {
		$("#agregar_aut").before('<input type="text" name="nuevoaut'+cant_autor+'" class="form-control nuevo_autor">');
		cant_autor = cant_autor + 1;
		$("#cant_autor").val(cant_autor);
	});

	///**************AGREGAR INTERESES**********************
	$("#otrof").click(function () {
	    if($("#otrof").is(":checked")){
		    $("#otrof").after('<input type="text" name="onuevo" id="onuevo" class="form-control required">');
		}else{
		    $("#onuevo").remove();
		}
	});

    //****************BUSCAR LIBROS**************************
    $("#btod").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#todoculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#todoculto").addClass("visible");
        $("#todoculto").removeClass("no_visible");
    });

    $("#bisbn").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#isbnoculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#isbnoculto").addClass("visible");
        $("#isbnoculto").removeClass("no_visible");
    });

    $("#btitulo").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#tituloculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#tituloculto").addClass("visible");
        $("#tituloculto").removeClass("no_visible");
    });

    $("#bautor").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#autoroculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#autoroculto").addClass("visible");
        $("#autoroculto").removeClass("no_visible");
    });

    $("#bedit").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#editoculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#editoculto").addClass("visible");
        $("#editoculto").removeClass("no_visible");
    });

    $("#bmodal").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#modaloculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#modaloculto").addClass("visible");
        $("#modaloculto").removeClass("no_visible");
    });

    $("#bcat").click(function(){
        $('.dropdown.open .dropdown-toggle').dropdown('toggle');
        $(".visible").hide("fast");
        $("#cateoculto").slideDown();
        $(".visible").removeClass("visible").addClass("no_visible");
        $("#cateoculto").addClass("visible");
        $("#cateoculto").removeClass("no_visible");
    });

    //****************AGREGAR LIBRO******************

    $("#selmodalidad").on('change', function () {
        var valueSelected = this.value;
        if (valueSelected == 'VEN'){
            $("#precioculto").show();
            $("#inputpre").attr("required", true);
        }else{
            $("#precioculto").hide();
            $("#inputpre").attr("required", false);
        }
    });

    $("#CambiarCategoria").on('change', function () {
        var valueSelected = this.value;
        if (valueSelected == '1'){
            $("#anoculto").show();
            $("#selectano").attr("required", true);
        }else{
            $("#anoculto").hide();
            $("#selectano").attr("required", false);
        }
    });


    ///**************HISTORIAL**********************
    $("#esolicitudesExi").click(function () {
      if ($("#solrecexi").is( ":visible" ) ) {
        $("#rsolicitudesExi").removeClass("active");
        $("#solrecexi").hide();
      } else {
        if ($("#solrecesp").is( ":visible" ) ) {
            $("#rsolicitudesEsp").removeClass("active");
            $("#solrecesp").hide();
        }else{
            if ($("#solenvesp").is( ":visible" ) ) {
                $("#esolicitudesEsp").removeClass("active");
                $("#solenvesp").hide();
            }
        }
      }

      if ($("#solenvexi").is( ":hidden" ) ) {
        $("#esolicitudesExi").addClass("active");
        $("#solenvexi").slideDown();
      } else {
        $("#esolicitudesExi").removeClass("active");
        $("#solenvexi").hide();
      }
    });

    $("#rsolicitudesExi").click(function () {
      if ($("#solrecesp").is( ":visible" ) ) {
        $("#rsolicitudesEsp").removeClass("active");
        $("#solrecesp").hide();
      } else {
        if ($("#solenvesp").is( ":visible" ) ) {
            $("#esolicitudesEsp").removeClass("active");
            $("#solenvesp").hide();
        }else{
            if ($("#solenvexi").is( ":visible" ) ) {
                $("#esolicitudesExi").removeClass("active");
                $("#solenvexi").hide();
            }
        }
      }

      if ($("#solrecexi").is( ":hidden" ) ) {
        $("#rsolicitudesExi").addClass("active");
        $("#solrecexi").slideDown();
      } else {
        $("#rsolicitudesExi").removeClass("active");
        $("#solrecexi").hide();
      }
    });

    $("#esolicitudesEsp").click(function () {
      if ($("#solrecexi").is( ":visible" ) ) {
        $("#rsolicitudesExi").removeClass("active");
        $("#solrecexi").hide();
      } else {
        if ($("#solrecesp").is( ":visible" ) ) {
            $("#rsolicitudesEsp").removeClass("active");
            $("#solrecesp").hide();
        }else{
            if ($("#solenvexi").is( ":visible" ) ) {
                $("#esolicitudesExi").removeClass("active");
                $("#solenvexi").hide();
            }
        }
      }

      if ($("#solenvesp").is( ":hidden" ) ) {
        $("#esolicitudesEsp").addClass("active");
        $("#solenvesp").slideDown();
      } else {
        $("#esolicitudesEsp").removeClass("active");
        $("#solenvesp").hide();
      }
    });

    $("#rsolicitudesEsp").click(function () {
      if ($("#solrecexi").is( ":visible" ) ) {
        $("#rsolicitudesExi").removeClass("active");
        $("#solrecexi").hide();
      } else {
        if ($("#solenvexi").is( ":visible" ) ) {
            $("#esolicitudesExi").removeClass("active");
            $("#solenvexi").hide();
        }else{
            if ($("#solenvesp").is( ":visible" ) ) {
                $("#esolicitudesEsp").removeClass("active");
                $("#solenvesp").hide();
            }
        }
      }

      if ($("#solrecesp").is( ":hidden" ) ) {
        $("#rsolicitudesEsp").addClass("active");
        $("#solrecesp").slideDown();
      } else {
        $("#rsolicitudesEsp").removeClass("active");
        $("#solrecesp").hide();
      }
    });

    ///**************CATEGORIZACION**********************
   

    $("#todo").click(function () {
      if ($("#lcompra").is( ":visible" ) ) {
        $("#compra").removeClass("active");
        $("#lcompra").addClass("oculto");
        $("#lcompra").hide();
        $
      } else {
        if ($("#ldonacion").is( ":visible" ) ) {
            $("#donacion").removeClass("active");
            $("#ldonacion").addClass("oculto");
            $("#ldonacion").hide();
            
        }else{
            if ($("#lintercambio").is( ":visible" ) ) {
                $("#intercambio").removeClass("active");
                $("#lintercambio").addClass("oculto");
                $("#lintercambio").hide();
                
            }
        }
      }

      if ($("#ltodo").is( ":hidden" ) ) {
        $("#todo").addClass("active");
        $("#ltodo").removeClass("oculto");
        window.location = "http://localhost:3000/libroteca/libros/listar_libros_completos/2";
        $("#ltodo").slideDown();
      }
    });

    $("#intercambio").click(function () {
      if ($("#lcompra").is( ":visible" ) ) {
        $("#compra").removeClass("active");
        $("#lcompra").addClass("oculto");
        $("#lcompra").hide();
      } else {
        if ($("#ldonacion").is( ":visible" ) ) {
            $("#donacion").removeClass("active");
            $("#ldonacion").addClass("oculto");
            $("#ldonacion").hide();
        }else{
            if ($("#ltodo").is( ":visible" ) ) {
                $("#todo").removeClass("active");
                $("#ltodo").addClass("oculto");
                $("#ltodo").hide();
            }
        }
      }

      if ($("#lintercambio").is( ":hidden" ) ) {
        $("#intercambio").addClass("active");
        $("#lintercambio").removeClass("oculto");
        window.location = "http://localhost:3000/libroteca/libros/listar_libros_completos/2";
        $("#lintercambio").slideDown();
        
      }
    });

    $("#compra").click(function () {
      if ($("#ltodo").is( ":visible" ) ) {
        $("#todo").removeClass("active");
        $("#ltodo").addClass("oculto");
        $("#ltodo").hide();
      } else {
        if ($("#ldonacion").is( ":visible" ) ) {
            $("#donacion").removeClass("active");
            $("#ldonacion").addClass("oculto");
            $("#ldonacion").hide();
        }else{
            if ($("#lintercambio").is( ":visible" ) ) {
                $("#intercambio").removeClass("active");
                $("#lintercambio").addClass("oculto");
                $("#lintercambio").hide();
            }
        }
      }

      if ($("#lcompra").is( ":hidden" ) ) {
        $("#compra").addClass("active");
        $("#lcompra").removeClass("oculto");
        window.location = "http://localhost:3000/libroteca/libros/listar_libros_completos/2";
        $("#lcompra").slideDown();
        
      }
    });

    $("#donacion").click(function () {
      if ($("#lcompra").is( ":visible" ) ) {
        $("#compra").removeClass("active");
        $("#lcompra").addClass("oculto");
        $("#lcompra").hide();
      } else {
        if ($("#ltodo").is( ":visible" ) ) {
            $("#todo").removeClass("active");
            $("#ltodo").addClass("oculto");
            $("#ltodo").hide();
        }else{
            if ($("#lintercambio").is( ":visible" ) ) {
                $("#intercambio").removeClass("active");
                $("#lintercambio").addClass("oculto");
                $("#lintercambio").hide();
            }
        }
      }

      if ($("#ldonacion").is( ":hidden" ) ) {
        $("#donacion").addClass("active");
        $("#ldonacion").removeClass("oculto");
        window.location = "http://localhost:3000/libroteca/libros/listar_libros_completos/2";
        $("#ldonacion").slideDown();
        
      }
    });

	

	///**************VALIDACIONES**********************
     
    $('#registrarUsuario').bootstrapValidator({
        	message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
            fields: {
                nombre: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, introduzca su nombre'
                        },
                        stringLength: {
                            min: 3,
                            max: 40,
                            message: 'El nombre debe tener como mínimo 3 caracteres y como máximo 40'
                        },
                        regexp:{
                        	regexp: /^[A-Za-z-\s]+$/,
                        	 message: 'Su nombre sólo puede contener letras y espacios'
                    	}
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, introduzca su correo electrónico'
                        },
                        emailAddress: {
                            message: 'Por favor, introduzca un correo electrónico válido'
                        }
                    }
                },
                clave: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, introduzca su contraseña'
                        },
                        stringLength: {
	                        min: 6,
	                        max: 20,
	                        message: 'Su contraseña debe contener al menos 6 caracteres y máximo 20'
	                    },
	                    regexp: {
	                        regexp: /^[a-zA-Z0-9_\.]+$/,
	                        message: 'Por favor, introduzca una contraseña válida'
	                    }
                    }
                },
                telefono: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, introduzca su teléfono'
                        },
                        phone: {
	                        country: 'VE',
	                        message: 'Por favor, introduzca su número de teléfono, incluyendo el código local o celular (0212,0412,0414,0424,0416)'
	                    }
                    }
                }
            }
        });

	$('#iniciarSesion').bootstrapValidator({
    	message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, introduzca su correo electrónico'
                    },
                    emailAddress: {
                        message: 'Por favor, introduzca un correo electrónico válido'
                    }
                }
            },
            clave: {
                validators: {
                    notEmpty: {
                        message: 'Por favor, introduzca su contraseña'
                    }
                }
            }
        }
    });

    $('#recordarClave').bootstrapValidator({
    	message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'email': {
                validators: {
                    notEmpty: {
                        message: 'Por favor, introduzca su correo electrónico'
                    },
                    emailAddress: {
                        message: 'Por favor, introduzca un correo electrónico válido'
                    }
                }
            }
        }
    });

    $('#AgregarLibroNuevo').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'titulo': {
                validators: {
                   notEmpty: {
                        message: 'Debe introducir un título para el libro'
                    },
                }
            },
            'ano': {
                validators: {
                    numeric: {
                        message: 'El año de publicación debe ser un número',
                        thousandsSeparator: '',
                        decimalSeparator: '.'
                    },
                    notEmpty: {
                        message: 'Debe introducir el año de publicación del libro'
                    }
                }
            },
            'autor': {
                validators: {
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'El nombre del autor sólo debe contener letras y espacios'
                    },
                    notEmpty: {
                        message: 'Debe introducir el nombre del autor del libro'
                    }
                }
            },
            'precio': {
                validators: {
                    numeric: {
                        message: 'El precio debe ser númerico y los decimales deben estar separados con "."',
                        thousandsSeparator: '',
                        decimalSeparator: '.'
                    }
                }
            },
            'editorial': {
                validators: {
                    regexp: {
                        regexp: /^[a-z\s]+$/i,
                        message: 'El nombre de la editorial, sólo debe contener letras y espacios'
                    },
                    notEmpty: {
                        message: 'Debe introducir el nombre de la editorial'
                    }
                }
            }

        }
    });

    $('#buscar_isbnn').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'isbn': {
                validators: {
                   notEmpty: {
                        message: 'Debe introducir el ISBN del libro'
                    },
                    stringLength: {
                        min: 10,
                        message: 'El ISBN debe tener al menos 10 dígitos'
                    }
                }
            }
        }
    });
    
    $('#AgregarListaDeDeseos').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'isbn': {
                validators: {
                   notEmpty: {
                        message: 'Debe introducir el ISBN del libro'
                    },
                    stringLength: {
                        min: 10,
                        message: 'El ISBN debe tener al menos 10 dígitos'
                    }
                }
            },
            'titulo': {
                validators: {
                    notEmpty: {
                        message: 'Debe introducir el título del libro'
                    }
              
                }
            }
        }
    });

    $('#AgregarAmigosEditar').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {        
            'amigo[]': {
                validators: {
                    choice: {
                        min: 1,
                        message: 'Debe agregar al menos un amigo'
                    }
                }
            }
        }
    });

    $('#CrearGrupoLectura').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'nombre': {
                validators: {
                    notEmpty: {
                        message: 'Debe introducir un nombre para el grupo de lectura'
                    }
              
                }
            },
            'fecha_fin':{
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'El formato de la fecha debe ser yyyy-mm-dd'
                    },
                    notEmpty: {
                        message: 'Debe introducir la fecha del evento'
                    }
                }
            },
            'amigo[]': {
                validators: {
                    choice: {
                        min: 1,
                        message: 'Debe agregar al menos un amigo al grupo de lectura'
                    }
                }
            }
        }
    });

  

    $('#CambiarClaveUsuario').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'clave_anterior': {
                validators: {
                    notEmpty: {
                        message: 'Por favor, introduzca su contraseña'
                    }
              
                }
            },
            'clave_nueva': {
                validators: {
                    notEmpty: {
                        message: 'Debe introducir su nueva contraseña'
                    },
                    stringLength: {
                        min: 6,
                        max: 20,
                        message: 'Su contraseña debe contener al menos 6 caracteres y máximo 20'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_\.]+$/,
                        message: 'Por favor, introduzca una contraseña válida'
                    }
                }
            },
            'clave_repetida': {
                validators: {
                    identical: {
                        field: 'clave_nueva',
                        message: 'Las contraseñas no coinciden, por favor, inténtelo nuevamente'
                    }
                }
            }
        }
    });

    $('#ComentarioGrupoLectura').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'comentario': {
                validators: {
                    notEmpty: {
                        message: 'El comentario no puede estar vacío'
                    }
                }
            }
        }
    });

    $('#EditarUsuario').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'nombre': {
                validators: {
                    notEmpty: {
                        message: 'Debe introducir su nombre'
                    }
                }
            },
            'direccion': {
                validators: {
                    notEmpty: {
                        message: 'Debe introducir una dirección'
                    }
                }
            },
            'telefono': {
                validators: {
                    notEmpty: {
                        message: 'La contraseña es requerida'
                    },
                    phone: {
                        country: 'VE',
                        message: 'Debe introducir un formato de teléfono válido'
                     }
                }
            },
            'email':{
                validators: {
                    notEmpty: {
                        message: 'Debe introducir un correo electrónico'
                    },
                    emailAddress: {
                        message: 'Debe introducir una dirección de correo válida'
                    }
                }
            }
        }
    });


    $('#EditarGrupo').bootstrapValidator({
        message: 'Este valor no es válido, por favor, inténtelo nuevamente',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'texto_editado': {
                validators: {
                    notEmpty: {
                        message: 'Este campo es requerido'
                    }
                }
            },
            'fecha_fin':{
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'El formato de la fecha debe ser yyyy-mm-dd'
                    },
                    notEmpty: {
                        message: 'Debe introducir la fecha del evento'
                    }

                }
            }
        }
    });

});




//**************MENU********************************
( function( $ ) {
$( document ).ready(function() {
	
    $('#sidebar li.has-sub>a').on('click', function(){
		
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

	$('#sidebar>ul>li.has-sub>a').append('<span class="holder"></span>');
	
	(function getColor() {
		var r, g, b;
		var textColor = $('#sidebar').css('color');
		textColor = textColor.slice(4);
		r = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		g = textColor.slice(0, textColor.indexOf(','));
		textColor = textColor.slice(textColor.indexOf(' ') + 1);
		b = textColor.slice(0, textColor.indexOf(')'));
		var l = rgbToHsl(r, g, b);
		if (l > 0.7) {
			$('#sidebar>ul>li>a').css('text-shadow', '0 1px 1px rgba(0, 0, 0, .35)');
			$('#sidebar>ul>li>a>span').css('border-color', 'rgba(0, 0, 0, .35)');
		}
		else
		{
			$('#sidebar>ul>li>a').css('text-shadow', '0 1px 0 rgba(255, 255, 255, .35)');
			$('#sidebar>ul>li>a>span').css('border-color', 'rgba(255, 255, 255, .35)');
		}
	})();

	function rgbToHsl(r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0;
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return l;
	}
});
} )( jQuery );



