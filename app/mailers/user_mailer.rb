class UserMailer < ApplicationMailer
	default from: 'libroteca@no-responder.com'

	def olvido_clave(usuario, clave) #correo que notifica el cambio de clave por olvido por parte de un usuario
		@usuario=usuario
		@clave= clave
		mail(to: usuario.email, subject: 'Clave para ingresar a Libroteca') # esta instrucción indica a que correo envía la notificación
	end

	def enviar_notificacion(usuario,descripcion,tipo_notificacion)
		@usuario= usuario
		@descripcion= descripcion
		@tipo_notificacion= TipoNotificacion.find(tipo_notificacion)
		mail(to: @usuario.email, subject: '[Libroteca] '+@tipo_notificacion.descripcion)
	end

  def aceptar_solicitud(usuariosol,usuariodue,libro)
    @usuariosol= usuariosol
    @usuariodue= usuariodue
    @libro= libro
    mail(to: @usuariodue.email, subject: '[Libroteca] Datos del Solicitante de tu Libro')
  end

  def aceptar_solicitud2(usuariosol,usuariodue,libro)
    @usuariosol= usuariosol
    @usuariodue= usuariodue
    @libro= libro
    mail(to: @usuariosol.email, subject: '[Libroteca] Datos del Responsable de tu Libro')
  end

end
