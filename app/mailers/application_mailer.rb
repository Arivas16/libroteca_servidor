class ApplicationMailer < ActionMailer::Base
  default from: "libroteca@no-responder.com"
  layout 'mailer'
end
