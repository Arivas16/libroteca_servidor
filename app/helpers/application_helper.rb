module ApplicationHelper

	def editable_texto_usuario(titulo,texto_a_mostrar, nombre_del_campo, identificador,coleccion)
		@titulo = titulo
		@texto_a_mostrar = texto_a_mostrar
		@nombre_del_campo = nombre_del_campo
		@identificador = identificador
		@coleccion = coleccion
		render "helper/editable_texto_usuario"
	end

	def editable_texto_libro(titulo,texto_a_mostrar, nombre_del_campo, identificador, coleccion, modal)
		@titulo = titulo
		@texto_a_mostrar = texto_a_mostrar
		@nombre_del_campo = nombre_del_campo
		@identificador = identificador
		@coleccion = coleccion
		@modal= modal
		render "helper/editable_texto_libro"
	end

	def paginador_de_noticias(coleccion)
		@coleccion = coleccion
		render "helper/paginador_de_noticias"
	end

	def listar_subcategoria_libros(coleccion,tipo_modalidad,subcategoria)
		@coleccion = coleccion
		@tipo_modalidad = tipo_modalidad
		@subcategoria = subcategoria
		render "helper/listar_subcategoria_libros"
	end

	def editable_texto_grupo(titulo,texto_a_mostrar, nombre_del_campo, identificador)
		@titulo = titulo
		@texto_a_mostrar = texto_a_mostrar
		@nombre_del_campo = nombre_del_campo
		@identificador = identificador
		render "helper/editable_texto_grupo"
	end

	def portada_de_libro(titulo)
		@titulo = titulo
		render "helper/portada_de_libro"
	end
	
	
	
	
end
