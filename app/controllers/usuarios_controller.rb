class UsuariosController < ApplicationController
  skip_before_filter :verify_authenticity_token
	layout 'interno'
  autocomplete :libro, :isbn
  autocomplete :libro, :titulo, :full => true
  autocomplete :autor, :nombre, :full => true
  autocomplete :libro, :editorial, :full => true

  def vista_buscar_amigos
  end

  def buscar_amigos
    expr = /^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z])+$/

    if expr.match(params[:amigo]) #es un correo
      @amigos = Usuario.where("email LIKE ?", "%#{params[:amigo]}%")
    else # es un nombre
      @amigos = Usuario.where("nombre LIKE ?", "%#{params[:amigo]}%")
    end

  end

	def registrar_usuario
		usuario = Usuario.where(email: params[:email]).take
    if usuario
      flash[:mensaje_error] = "El correo ya está asociado a otro usuario, por favor, verifique e inténtelo nuevamente"
      puts "no creó el usuario, ya existe el correo"
    else
      intereses= ""
      usuario= Usuario.new
      usuario.nombre = params[:nombre].capitalize
      usuario.email = params[:email]
      usuario.telefono = params[:telefono]
      usuario.direccion= params[:direccion]
      usuario.clave = Usuario.md5(params[:clave])
      usuario.picture = params[:picture]
      if params[:ciencias] == "on"
        intereses= intereses+"ciencias"
      end
      if params[:literatura] == "on"
        intereses= intereses+"-literatura"
      end
      if params[:matematica] == "on"
        intereses= intereses+"-matemática"
      end
      if params[:sociales] == "on"
        intereses= intereses+"-sociales"
      end
      if params[:historia] == "on"
        intereses= intereses+"-historia"
      end
      if params[:geografia] == "on"
        intereses= intereses+"-geografía"
      end
      if params[:fisica] == "on"
        intereses= intereses+"-física"
      end
      if params[:quimica] == "on"
        intereses= intereses+"-química"
      end
      if params[:ingles] == "on"
        intereses= intereses+"-inglés"
      end
      if params[:computacion] == "on"
        intereses= intereses+"-computación"
      end
      if params[:onuevo]
        intereses= intereses+"-"+params[:onuevo].downcase
      end
      usuario.intereses = intereses
      usuario.comunidad_educativa= params[:comunidad]
      usuario.tipo_configuracion_id= params[:configuracion]
      usuario.puntuacion = 0
      if usuario.save
            flash[:mensaje]= "El usuario ha sido creado exitosamente, por favor, inicie sesión"
            puts "creo el usuario"
        else
          flash[:mensaje_error]= "Ocurrió un error al crear su usuario, por favor, inténtelo nuevamente"
          puts "no guardo el usuario"
        end
    end
    redirect_to controller: :bienvenida, action: :index
	end

	def vista_editar_usuario
    if session[:id]!=nil
  		@usuario = Usuario.find(session[:id])
      @intereses = @usuario.intereses.split("-")
      @tipo_configuracion = TipoConfiguracion.all
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
	end

  def editar_picture
    if session[:id]!=nil
      @persona = Usuario.find(session[:id])
      if params[:picture]
        @persona.picture= params[:picture]
        if !@persona.save
          flash[:mensaje_error]="Ocurrió un error al actualizar la foto, por favor, inténtelo nuevamente"
        end
        redirect_to controller: :usuarios, action: :vista_editar_usuario
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

	def editar_usuario
    if session[:id]!=nil
      @persona = Usuario.find(session[:id])
      @campo = params[:nombre_del_campo]
      @identificador = params[:id]
      @correo = false

      if params[:nombre]
      	@persona.nombre = params[:nombre].capitalize
      	@texto = params[:nombre].capitalize
      end

      if params[:email]
        @email = Usuario.where(email:params[:email]).take
        if @email
          
          @correo = true
        else
          @persona.email = params[:email]
          @texto = params[:email]
        end
      end

      if params[:telefono]
      	@persona.telefono = params[:telefono]
      	@texto = params[:telefono]
      end

      if params[:direccion]
      	@persona.direccion = params[:direccion]
      	@texto = params[:direccion]
      end

      if params[:tipo_configuracion_id]
        @persona.tipo_configuracion_id = params[:tipo_configuracion_id]
        @texto = TipoConfiguracion.find(params[:tipo_configuracion_id]).descripcion
      end


      if @persona.save
         tipo_noticia = TipoNoticia.find("PERF")
         @noticia = tipo_noticia.descripcion
         @tipo_noticia_id= tipo_noticia.id
         @generador_noticia_id = @persona.id
      else
        @error='Ocurrió un error al actualizar los datos, por favor, inténtelo nuevamente'
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def editar_intereses
    if session[:id]!=nil
      @persona = Usuario.find(session[:id])
      intereses=''
      if params[:ciencias] == "on"
        intereses= intereses+"ciencias"
      end
      if params[:literatura] == "on"
        intereses= intereses+"-literatura"
      end
      if params[:matematica] == "on"
        intereses= intereses+"-matemática"
      end
      if params[:sociales] == "on"
        intereses= intereses+"-sociales"
      end
      if params[:historia] == "on"
        intereses= intereses+"-historia"
      end
      if params[:geografia] == "on"
        intereses= intereses+"-geografía"
      end
      if params[:fisica] == "on"
        intereses= intereses+"-física"
      end
      if params[:quimica] == "on"
        intereses= intereses+"-química"
      end
      if params[:ingles] == "on"
        intereses= intereses+"-inglés"
      end
      if params[:computacion] == "on"
        intereses= intereses+"-computación"
      end
      if params[:onuevo]
        intereses= intereses+"-"+params[:onuevo].downcase
      end
      @persona.intereses = intereses

     
      if @persona.save
        tipo_noticia = TipoNoticia.find("PERF")
         @noticia = tipo_noticia.descripcion
         @tipo_noticia_id= tipo_noticia.id
         @generador_noticia_id = @persona.id
      else
        @error='Ocurrió un error al actualizar los datos, por favor, inténtelo nuevamente'
      end

      redirect_to controller: :usuarios, action: :vista_editar_usuario
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def perfil
    if session[:id]!=nil
    	@usuario = Usuario.find(params[:id])
      @amigos= []
      @noticias = Noticia.where(usuario_id:params[:id]).paginate(page: params[:page], per_page: 4)
      @amigos= UsuarioAmigo.where(usuario_id:params[:id], tipo_amigo_id: "ACEP")
      @amigos= @amigos + UsuarioAmigo.where(amigo_id:params[:id], tipo_amigo_id: "ACEP")
      @es_amigo= nil
      @sol=nil
      if params[:id].to_i == session[:id]
        @es_amigo= true
      else
        @sol= UsuarioAmigo.where(usuario_id:session[:id],amigo_id:params[:id],tipo_amigo_id:'NOR').take
        @es_amigo= UsuarioAmigo.where(usuario_id:session[:id],amigo_id:params[:id],tipo_amigo_id:'ACEP').take
        if @es_amigo == nil
          @es_amigo= UsuarioAmigo.where(usuario_id:params[:id],amigo_id:session[:id],tipo_amigo_id: 'ACEP').take
        end
        if @sol==nil
          @sol=UsuarioAmigo.where(usuario_id:params[:id],amigo_id:session[:id],tipo_amigo_id: 'NOR').take
        end
      end
    else
      flash[:mensaje_error]="Para ver el perfil de este usuario, por favor, regístrese en la aplicación"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def mis_amigos
    if session[:id]!=nil
      @usuario = Usuario.find(params[:id])
      ami = Usuario.amigos_id(@usuario.id)
      @amigos = Array.new
      ami.each do |a|
        @amigos.push(Usuario.find(a))
      end
      @misamigos = @amigos.paginate(:page => params[:page], :per_page => 8)
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def misamigos
    if session[:id]!=nil
      amig=false
      @usuario = Usuario.find(params[:id])
      @amigosrec= UsuarioAmigo.where(usuario_id:params[:id], tipo_amigo_id: "ACEP").paginate(page: params[:page], per_page: 4)
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def misamigos_enviados
    if session[:id]!=nil
      amig=false
      @usuario = Usuario.find(params[:id])
      @amigosenv=[]
      @amigue= UsuarioAmigo.where(amigo_id:params[:id], tipo_amigo_id: "ACEP").paginate(page: params[:page], per_page: 4)
      @amigue.each do |ae|
        @amigosenv.push(Usuario.find(ae.usuario_id))
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def agregar_amigo
    if session[:id]!=nil
      ua= UsuarioAmigo.new
      ua.usuario_id= params[:amigo_id]
      ua.amigo_id= session[:id]
      ua.tipo_amigo_id= "NOR"
      u= Usuario.find(session[:id])
      if ua.save
        flash[:mensaje] = "La solicitud de amistad ha sido enviada exitosamente"
        u.puntuacion= u.puntuacion+1
        u.save
        descripcion= "El usuario "+ua.usuario.nombre+" te agregó como amigo"
        crear_notificacion('INVT-A',params[:amigo_id],descripcion,session[:id])
      else
        flash[:mensaje_error] = "Ocurrió un error al enviar la solicitud, por favor, inténtelo nuevamente"
      end
      redirect_to controller: :usuarios, action: :solicitudes_amigos_enviadas
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def aceptar_amigo
    if session[:id]!=nil
      ua= UsuarioAmigo.find([session[:id],params[:amigo_id]])
      ua.tipo_amigo_id= 'ACEP'
      u= Usuario.find(session[:id])
      if ua.save
        flash[:mensaje] = "La solicitud de amistad ha sido aceptada exitosamente"
        descripcion= "El usuario "+u.nombre+" aceptó tu solicitud de amistad"
        crear_notificacion('AMIGO',params[:amigo_id],descripcion,session[:id])
        u.puntuacion= u.puntuacion + 1
        u.save
        mostrar_noticia('AMIG',params[:amigo_id])
      else
        flash[:mensaje_error] = "Ocurrió un error al aceptar la solicitud, por favor, inténtelo nuevamente"
        redirect_to controller: :usuarios, action: :solicitudes_amigos_recibidas
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def rechazar_amigo
    if session[:id]!=nil
      ua= UsuarioAmigo.find([session[:id],params[:amigo_id]])
      u= Usuario.find(session[:id])
      if ua.destroy
        flash[:mensaje] = "La solicitud de amistad ha sido eliminada exitosamente"
        descripcion= "El usuario "+u.nombre+" rechazó tu solicitud de amistad"
        crear_notificacion('A-RECHAZADA',params[:amigo_id],descripcion,session[:id])
      else
        flash[:mensaje_error] = "Ocurrió un error al rechazar la solicitud, por favor, inténtelo nuevamente"
      end
      redirect_to controller: :usuarios, action: :solicitudes_amigos_recibidas
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def cancelar_amigo
    if session[:id]!=nil
      ua= UsuarioAmigo.find([params[:amigo_id],session[:id]])
      if ua.destroy
         flash[:mensaje] = "Usted canceló la solicitud de amistad exitosamente"
      else
        flash[:mensaje_error] = "Ocurrió un error al cancelar la solicitud, por favor, inténtelo nuevamente"
      end
      redirect_to controller: :usuarios, action: :solicitudes_amigos_enviadas
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def historial
    if session[:id]!=nil
      amig=false
      @usuario = Usuario.find(params[:id])
      ami = Usuario.amigos_id(@usuario.id)
      if session[:id] == params[:id].to_i
        amig=true
      end
      ami.each do |a|
        if session[:id]== a && amig==false
          amig=true
        end
      end
      if amig==false
        flash[:mensaje_error]="Para ver el historial de este usuario, debe agregarlo como amigo"
        redirect_to controller: :usuarios, action: :perfil, id: params[:id]
      end
      @usuariosresp=[]
      @usuariosrespe=[]
      @rsolicitudesexi= Solicitud.where(usuario_dueno_id: @usuario.id, estado_solicitud_id: "REALIZADA").all
      @rsolicitudesesp= Solicitud.where(usuario_dueno_id: @usuario.id, estado_solicitud_id: "ENVIADA").all
      @esolicitudesexi= Solicitud.where(usuario_solicitante_id: @usuario.id, estado_solicitud_id: "REALIZADA").all
      @esolicitudesesp= Solicitud.where(usuario_solicitante_id: @usuario.id, estado_solicitud_id: "ENVIADA").all
      @esolicitudesexi.each do |sol|
        @usuariosresp.push(Usuario.find(sol.usuario_dueno_id))
      end
      @esolicitudesesp.each do |sol|
        @usuariosrespe.push(Usuario.find(sol.usuario_dueno_id))
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def solicitudes_amigos_recibidas
    if session[:id]!=nil
      @solicitudes= UsuarioAmigo.where(usuario_id: session[:id], tipo_amigo_id: 'NOR')
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index 
    end
  end

  def solicitudes_amigos_enviadas
    if session[:id]!=nil
      @solicitudes= UsuarioAmigo.where(amigo_id: session[:id], tipo_amigo_id: 'NOR')
      @usuarios =[]
      @solicitudes.each do |sol|
        @usuarios.push(Usuario.find(sol.usuario_id))
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

	def subir_imagen
    if session[:id]!=nil
  		@usuario = FotoUsuario.find(session[:id])
    	puts "**********************************************************"

    	@usuario.avatar_from_url(params[:foto_usuario][:picture])
    	if @usuario.save
      		puts "no hubo problema"
      		render "usuarios/vista_editar_usuario"
    	else
    		puts "hubo un problema"
      		render "usuarios/vista_editar_usuario"
    	end
    else
      flash[:mensaje_error]="Por favor inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
	end

  def actualizar_clave
    if session[:id]!=nil
      @usuario = Usuario.find(session[:id])
      if @usuario.clave == Usuario.md5(params[:clave_anterior])
        if params[:clave_nueva] ==  params[:clave_repetida]
          @usuario.clave = Usuario.md5(params[:clave_nueva])
          @usuario.save
        else
          flash[:mensaje_error] = "Las contraseñas no coinciden, por favor, inténtelo nuevamente"
        end
      else
        flash[:mensaje_error] = "Su contraseña actual es incorrecta, por favor, inténtelo nuevamente"
      end
      flash[:mensaje] = "Su contraseña ha sido actualizada exitosamente"
      redirect_to controller: :usuarios , action: :vista_editar_usuario
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def eliminar_usuario
    if session[:id]!=nil
      Usuario.find(session[:id]).destroy
      redirect_to controller: :bienvenida, action: :index
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def eliminar_amigo
    if session[:id]!=nil
      ua= UsuarioAmigo.find([session[:id],params[:amigo_id]])
      if ua==nil
        ua= UsuarioAmigo.find([params[:amigo_id],session[:id]])
      end
      if ua.destroy
        flash[:mensaje] = "El usuario ha sido eliminado de su lista de amigos exitosamente"
      else
        flash[:mensaje_error] = "Ocurrió un error al eliminar su amigo, por favor, inténtelo nuevamente"
      end
      amigos= UsuarioAmigo.where(usuario_id:session[:id], tipo_amigo_id: "ACEP")
      if amigos.count !=0
        redirect_to controller: :usuarios , action: :mis_amigos, id: session[:id]
      else
        redirect_to controller: :usuarios , action: :perfil, id: session[:id]
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end


  def mostrar_noticia(tipo_noticia,id)
    if session[:id]!=nil
      flash[:mensaje_noticia] = true
      @tipo_noticia = TipoNoticia.find(tipo_noticia)
      case tipo_noticia
        when 'AMIG'
          @texto= Usuario.find(id)
          @noticia= @tipo_noticia.descripcion+@texto.nombre
          redirect_to controller: :usuarios, action: :mis_amigos, id:session[:id], noticia: @noticia ,tipo_noticia_id: @tipo_noticia.id ,generador_noticia_id: id
        else
          puts "Ocurrio un error"
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

  def crear_notificacion(tipo_notificacion,usuario_id,descripcion,amigo_id)
    if session[:id]!=nil
      usuario= Usuario.find(usuario_id)
      if usuario.tipo_configuracion_id=='COR' || usuario.tipo_configuracion_id=='NOTyCOR'
        UserMailer.enviar_notificacion(usuario,descripcion,tipo_notificacion).deliver_later
      end
      notif= Notificacion.new
      notif.usuario_id= usuario_id
      notif.descripcion= descripcion
      notif.tipo_notificacion_id= tipo_notificacion
      notif.amigo_id= amigo_id
      notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
      if notif.save
        flash[:mensaje_notificacion]="La notificación ha sido enviada exitosamente"
        return true
      else
        flash[:mensaje_notificacion]="Ocurrió un error, por favor, inténtelo nuevamente"
        return false
      end
    else
      flash[:mensaje_error]="Por favor, inicie sesión"
      redirect_to controller: :bienvenida, action: :index
    end
  end

end

