class GrupoDeLecturaController < ActionController::Base
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true
	
	def vista_crear_grupo_lectura
		if session[:id]!=nil
			@usuario = Usuario.find(session[:id])
		      ami = Usuario.amigos_id(@usuario.id)
		      @amigos = Array.new
		      ami.each do |a|
		        @amigos.push(Usuario.find(a))
		      end
	    else
	    	flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
	    end
	end

	def crear_grupo_lectura
		if session[:id]!=nil
			tiempo = Time.now
			@grupo_lectura =GrupoLectura.create(nombre:params[:nombre],fecha_inicio:tiempo.strftime("%Y-%m-%d").to_s,fecha_fin:params[:fecha_fin], usuario_id:session[:id])
			params[:amigo].each do |a|
				GrupoLecturaUsuario.create(grupo_lectura_id:@grupo_lectura.id,usuario_id:a, tipo_amigo_id:"NOR")
			end
			
			flash[:mensaje] = "El grupo de lectura fue creado exitosamente"
			@usuarios = GrupoLecturaUsuario.where(grupo_lectura_id:@grupo_lectura.id)
			crear_notificacion("INVT-L",@usuarios,"El usuario "+Usuario.find(session[:id]).nombre+" te invito al grupo de lectura "+@grupo_lectura.nombre,@grupo_lectura.id)
			mostrar_noticia("GRUP",@grupo_lectura.id)
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def grupos_propios
		if session[:id]!=nil
			amig=false
			@usuario = Usuario.find(params[:id])
			ami = Usuario.amigos_id(@usuario.id)
			if session[:id] == params[:id].to_i
				amig=true
			end
			ami.each do |a|
				if session[:id]== a && amig==false
				  amig=true
				end
			end
			if amig==false
				flash[:mensaje_error]="Para ver los grupos de este usuario, debe agregarlo como amigo"
				redirect_to controller: :usuarios, action: :perfil, id: params[:id]
			end
			@mis_grupos = GrupoLectura.where(usuario_id:params[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def otros_grupos
		if session[:id]!=nil
			@otros_grupos = GrupoLecturaUsuario.where(usuario_id:session[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def vista_editar_grupo
		if session[:id]!=nil
			@grupo = GrupoLectura.find(params[:grupo_id])
			mis_amigos = Usuario.amigos_id(session[:id])
			l = GrupoLecturaUsuario.select(:usuario_id).where(grupo_lectura_id:params[:grupo_id],tipo_amigo_id:"ACEP")
			los_miembros = Array.new
			l.each do |b|
				los_miembros.push(b.usuario_id)
			end
			x = (mis_amigos - los_miembros)
			@amigos = Array.new
			x.each do |a|
				@amigos.push(Usuario.find(a))
			end
			if @grupo.usuario_id != session[:id]
				flash[:mensaje_error]="Usted sólo tiene permiso de editar los grupos de lectura que haya creado"
				redirect_to controller: :grupo_de_lectura, action: :grupos_propios, id:session[:id]
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def editar_grupo
		if session[:id]!=nil
			@grupo = GrupoLectura.find(params[:id])
			if @grupo.usuario_id != session[:id]
				flash[:mensaje_error]="Usted sólo tiene permiso de editar los grupos de lectura que haya creado"
				redirect_to controller: :grupo_de_lectura, action: :grupos_propios, id:session[:id]
			else
				@campo = params[:nombre_del_campo]
				if params[:fecha_fin]
					@texto = params[:fecha_fin]
					@grupo.fecha_fin = params[:fecha_fin]
				else
					@texto = params[:texto_editado]
					@grupo.nombre = params[:texto_editado]
				end
				
				@identificador = params[:id]
				#@grupo.send "#{params[:nombre_del_campo]}=", "#{params[:texto_editado]}"		
				if @grupo.save
					crear_notificacion('G-EDITO',GrupoLecturaUsuario.where(grupo_lectura_id:@grupo.id,tipo_amigo_id:"ACEP"),"El usuario "+Usuario.find(@grupo.usuario_id).nombre+" edito la información del grupo de lectura "+@grupo.nombre,@grupo.id)
				end
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def eliminar_grupo
		if session[:id]!=nil
			@grupo= GrupoLectura.find(params[:id])
			if @grupo.usuario_id != session[:id]
				flash[:mensaje_error]="Usted sólo tiene permiso de editar los grupos de lectura que haya creado"
				redirect_to controller: :grupo_de_lectura, action: :grupos_propios, id:session[:id]
			else
				@usuarios = GrupoLecturaUsuario.where(grupo_lectura_id:params[:id],tipo_amigo_id:"ACEP")
				crear_notificacion("ELI-G",@usuarios, "El usuario "+Usuario.find(session[:id]).nombre+" eliminó el grupo de lectura "+GrupoLectura.find(params[:id]).nombre,params[:id])
				GrupoLectura.find(params[:id]).destroy
				flash[:mensaje] = "El grupo de lectura ha sido eliminado exitosamente"
				redirect_to controller: :grupo_de_lectura , action: :grupos_propios , id:session[:id]
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def solicitudes_grupos_recibidas
		if session[:id]!=nil
			@solicitudes = GrupoLecturaUsuario.where(usuario_id:session[:id],tipo_amigo:"NOR")
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def rechazar_solicitud
		if session[:id]!=nil
			@grupo = GrupoLectura.find(params[:id])
			crear_notificacion("G-RECHAZADA",Usuario.find(@grupo.usuario_id),"El usuario "+Usuario.find(session[:id]).nombre+" rechazó la solicitud al grupo de lectura "+@grupo.nombre,@grupo.id)
			GrupoLecturaUsuario.where(grupo_lectura_id:params[:id],usuario_id:session[:id]).take.destroy
			flash[:mensaje] = "La invitación al grupo de lectura fue eliminada"
			redirect_to controller: :grupo_de_lectura, action: :solicitudes_grupos_recibidas
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def aceptar_solicitud
		if session[:id]!=nil
			@grupo = GrupoLectura.find(params[:id])
			crear_notificacion("G-ACEPTADA",Usuario.find(@grupo.usuario_id),"El usuario "+Usuario.find(session[:id]).nombre+" aceptó la solicitud al grupo de lectura "+@grupo.nombre,@grupo.id)
			@grupo_usuario = GrupoLecturaUsuario.where(grupo_lectura_id:params[:id],usuario_id:session[:id]).take
			@grupo_usuario.tipo_amigo_id = "ACEP"
			@grupo_usuario.save
			flash[:mensaje] = "Usted ahora pertence al grupo de lectura "+@grupo.nombre
			redirect_to controller: :grupo_de_lectura , action: :grupo, id: params[:id]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def grupo
		if session[:id]!=nil
			@grupo = GrupoLectura.find(params[:id])
			@comentarios = ComentarioGrupoLectura.where(grupo_lectura_id:params[:id]).order(fecha_creacion: :desc).paginate(page: params[:page], per_page: 10)
			if @grupo.usuario_id == session[:id]
				@es_miembro=true
			else
				@es_miembro1 = GrupoLecturaUsuario.where(grupo_lectura_id:params[:id],usuario_id:session[:id]).take
				if @es_miembro1==nil
					@es_miembro=false
				else
					@es_miembro=true
				end
			end
			mis_amigos = Usuario.amigos_id(session[:id])
			l = GrupoLecturaUsuario.select(:usuario_id).where(grupo_lectura_id:params[:id],tipo_amigo_id:"ACEP")
			los_miembros = Array.new
			l.each do |b|
				los_miembros.push(b.usuario_id)
			end
			x = (mis_amigos - los_miembros)
			@amigos = Array.new
			x.each do |a|
				@amigos.push(Usuario.find(a))
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def comentar_grupo
		if session[:id]!=nil
			@numero_comentario = ComentarioGrupoLectura.where(grupo_lectura_id:params[:grupo_id]).count
			@comentario = ComentarioGrupoLectura.create(descripcion:params[:comentario],fecha_creacion:Time.now,usuario_id:session[:id],grupo_lectura_id:params[:grupo_id])
			@usuario = Usuario.find(session[:id])
			@grupo = GrupoLectura.find(params[:grupo_id])
			@usuarios = GrupoLecturaUsuario.where("grupo_lectura_id = ? AND usuario_id != ? AND tipo_amigo_id = ?",params[:grupo_id],session[:id], "ACEP")
			if @grupo.usuario_id != session[:id]
				g = GrupoLecturaUsuario.new
				g.usuario_id = @grupo.usuario_id
				g.grupo_lectura_id = params[:grupo_id]
				@usuarios.push(g)
			end
			crear_notificacion("COMENTARIO-L",@usuarios ,"El usuario "+Usuario.find(session[:id]).nombre+" comentó el grupo de lectura "+@grupo.nombre,params[:grupo_id])
			redirect_to controller: :grupo_de_lectura, action: :grupo, id: params[:grupo_id]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def eliminar_comentario
		ComentarioGrupoLectura.find(params[:id]).destroy
		flash[:mensaje] = "El comentario fue eliminado exitosamente"
		redirect_to controller:"grupo_de_lectura", action:"grupo", id: params[:grupo_id]
	end

	def ver_miembros
		if session[:id]!=nil
			@miembro_grupo = GrupoLecturaUsuario.where(grupo_lectura_id:params[:id],tipo_amigo_id:"ACEP").paginate(page: params[:page], per_page: 10)
			@grupo = GrupoLectura.find(params[:id])		
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def agregar_miembros
		if session[:id]!=nil
			@usuarios = []
			params[:amigo].each do |a|
				if !(GrupoLecturaUsuario.where(grupo_lectura_id:params[:grupo_id],usuario_id:a,tipo_amigo_id:"NOR").take)
					@usuarios.push(GrupoLecturaUsuario.create(grupo_lectura_id:params[:grupo_id],usuario_id:a, tipo_amigo_id:"NOR"))
				end
			end
			crear_notificacion("INVT-L",@usuarios,"El usuario "+Usuario.find(session[:id]).nombre+" te invitó al grupo de lectura",params[:grupo_id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def eliminar_miembro_grupo
		if session[:id]!=nil
			@grupo= GrupoLectura.find(params[:grupo_id])
			if @grupo.usuario_id == session[:id]
				GrupoLecturaUsuario.where(grupo_lectura_id:params[:grupo_id],usuario_id:params[:miembro_id]).delete_all
				crear_notificacion("ELI-G-M",Usuario.find(params[:miembro_id]),"El usuario "+Usuario.find(session[:id]).nombre+" te eliminó de su grupo de lectura "+GrupoLectura.find(params[:grupo_id]).nombre,params[:grupo_id])
				flash[:mensaje] = "El usuario fue eliminado exitosamente del grupo de lectura"
				redirect_to controller: :grupo_de_lectura , action: :ver_miembros, id: params[:grupo_id]
			else
				flash[:mensaje_error]="Usted sólo tiene permiso de editar los grupos de lectura que haya creado"
				redirect_to controller: :grupo_de_lectura, action: :grupos_propios, id:session[:id]
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def pedir_invitacion
		if session[:id]==nil
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

	def mostrar_noticia(tipo_noticia,id)
		if session[:id]!=nil
		  	flash[:mensaje_noticia] = true
		  	@tipo_noticia = TipoNoticia.find(tipo_noticia)
		  	case tipo_noticia
				when "GRUP"
					@texto = GrupoLectura.find(id)
					@noticia = @tipo_noticia.descripcion+" "+@texto.nombre
					redirect_to controller: "grupo_de_lectura", action:"grupos_propios" ,id:session[:id], noticia: @noticia ,tipo_noticia_id: @tipo_noticia.id ,generador_noticia_id: id
				else
				  puts "Ocurrio un error"
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
  	end

	def crear_notificacion(tipo_notificacion,usuarios,descripcion,grupo_id)
  		@grupo = GrupoLectura.find(grupo_id)
  		if tipo_notificacion == "G-RECHAZADA" || tipo_notificacion == "G-ACEPTADA" || tipo_notificacion == "ELI-G-M"
  			 if usuarios.tipo_configuracion_id=='COR' || usuarios.tipo_configuracion_id=='NOTyCOR'
			      UserMailer.enviar_notificacion(usuarios,descripcion,tipo_notificacion).deliver_later
			    end
			    notif= Notificacion.new
			    notif.usuario_id= usuarios.id
			    notif.descripcion= descripcion
			    notif.tipo_notificacion_id= tipo_notificacion
			    notif.grupo_lectura_id= grupo_id
			    notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
			    notif.amigo_id = session[:id]
			    notif.save
  		else
			
	  		usuarios.each do |u|
				@usuario = Usuario.find(u.usuario_id)
				
			    if @usuario.tipo_configuracion_id=='COR' || @usuario.tipo_configuracion_id=='NOTyCOR'
			      UserMailer.enviar_notificacion(@usuario,descripcion,tipo_notificacion).deliver_later
			    end
			    notif= Notificacion.new
			    notif.usuario_id= u.usuario_id
			    notif.descripcion= descripcion
			    notif.tipo_notificacion_id= tipo_notificacion
			    notif.grupo_lectura_id= u.grupo_lectura_id
			    notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
			    if tipo_notificacion == "ELI-G" || tipo_notificacion == "G-EDITO" || tipo_notificacion == "COMENTARIO-L"
			    	notif.amigo_id = session[:id]
			    end
			    notif.save
			end
		end
	end

end

