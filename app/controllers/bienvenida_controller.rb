class BienvenidaController < ActionController::Base
	layout 'principal'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true

	def index
	end

	def informacion
	end

  	def busqueda_libros
  			@lista=[]
			@modalidades=TipoModalidad.all
			@categorias= Categoria.all
		    @subcategoriasEscolar = Subcategoria.where(categoria_id:1)
		    @subcategoriasUniversitario = Subcategoria.where(categoria_id:2)
			if params[:buscar]!=nil
				@palabra=params[:buscar].downcase
				@libros= Libro.where("titulo LIKE ?", "%#{@palabra}%").order("titulo ASC") # se consulta a la base de datos ordenando de forma ascendente	
				if @libros!=nil
					@libros.each do |lib|
						@librou= LibroUsuario.where(libro_id: lib.id).all
						@librou.each do |l|
							@lista.push(l)
						end
					end
					puts @lista.size()
					@pages= @lista.paginate(:page => params[:page], :per_page => 8)
				end
			else
				@lista=nil
			end
	end

	def busqueda_general
			@modalidades=TipoModalidad.all
			@categorias= Categoria.all
		    @subcategoriasEscolar = Subcategoria.where(categoria_id:1)
		    @subcategoriasUniversitario = Subcategoria.where(categoria_id:2)
			@libros=[]
			if params[:titulo]!=nil
				palabra=params[:titulo].downcase
				libs= Libro.where("titulo LIKE ?", "%#{palabra}%").order("titulo ASC") # se consulta a la base de datos ordenando de forma ascendente	
				if libs!=nil
					libs.each do |lib|
						librou= LibroUsuario.where(libro_id: lib.id).all
						librou.each do |l|
							@libros.push(l)
						end
					end
				end
			end
			if params[:isbn]
				palabra= params[:isbn].downcase
				libros= Libro.where("isbn LIKE ?", "%#{palabra}%")
				if libros
					libros.each do |lib|
						isbns= LibroUsuario.where(libro_id: lib.id)
						isbns.each do |l|
							@libros.push(l)
						end
					end
				end
			end
			if params[:autor]
				palabra= params[:autor].downcase
				autores= Autor.where("nombre LIKE ?", "%#{palabra}%")
				if autores
					autores.each do |au|
						libs=LibroUsuario.where(libro_id: au.libro_id)
						libs.each do |lib|
							@libros.push(lib)
						end
					end
				end
			end
			if params[:editorial]
				palabra= params[:editorial].downcase
				libs= Libro.where("editorial LIKE ?", "%#{palabra}%")
				if libs
					libs.each do |lib|
						libs2=LibroUsuario.where(libro_id: lib.id)
						libs2.each do |lib|
							@libros.push(lib)
						end
					end
				end
			end

			if params[:modalidad]
				libs= LibroUsuario.where(tipo_modalidad_id: params[:modalidad])
				libs.each do |lib|
					@libros.push(lib)
				end
			end

			if params[:categoria]
				if params[:categoria].to_i == 1
					libs= LibroUsuario.where(categoria_id: params[:categoria],subcategoria_id: params[:subcategoriaEscolar])
				else
					libs= LibroUsuario.where(categoria_id: params[:categoria],subcategoria_id: params[:subcategoriaUniversitario])
				end
				libs.each do |lib|
					@libros.push(lib)
				end
			end

			@pages= @libros.paginate(:page => params[:page], :per_page => 8)
	end

end

