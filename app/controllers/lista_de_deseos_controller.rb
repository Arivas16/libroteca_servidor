class ListaDeDeseosController < ActionController::Base
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true

	def vista_agregar_libro_lista
		if session[:id]==nil
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def agregar_libro_lista
		if session[:id]!=nil
			@libro = Libro.where(isbn:params[:isbn]).take
			@libro2 = Libro.where(titulo:params[:titulo]).take
			if @libro || @libro2
				if @libro
					ListaDeseos.create(libro_id:@libro.id,usuario_id:session[:id])
					@usuarios= ListaDeseos.where(libro_id: @libro.id)
					@libu= LibroUsuario.where(libro_id: @libro.id)
				else
					ListaDeseos.create(libro_id:@libro2.id,usuario_id:session[:id])
					@usuarios= ListaDeseos.where(libro_id: @libro2.id)
					@libu= LibroUsuario.where(libro_id: @libro2.id)
				end
				
				if @libu
					@libu.each do |lib|
						crear_notificacion('LIST-DES',session[:id],"El libro que acaba de agregar a su lista de deseos: "+lib.libro.titulo+", se encuentra disponible",lib.libro.id,lib.usuario_id)
						flash[:mensaje] = "!Este libro ya existe en Libroteca¡ revise sus notificaciones y solicítelo"
					end
				else
					flash[:mensaje] = "El libro ha sido agregado a su lista de deseos exitosamente"
				end
			else
				@nuevo_libro = Libro.create(isbn:params[:isbn],titulo:params[:titulo],lista_deseo:1)
				ListaDeseos.create(libro_id:@nuevo_libro.id,usuario_id:session[:id])
				flash[:mensaje] = "El libro ha sido agregado a su lista de deseos exitosamente"
			end
			
			redirect_to controller: :lista_de_deseos , action: :lista , id:session[:id]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def lista
		if session[:id]!=nil
			amig=false
			@usuario = Usuario.find(params[:id])
			ami = Usuario.amigos_id(@usuario.id)
			if session[:id] == params[:id].to_i
				amig=true
			end
			ami.each do |a|
				if session[:id]== a && amig==false
				  amig=true
				end
			end
			if amig==false
				flash[:mensaje_error]="Para ver la lista de deseos de este usuario, debe agregarlo como amigo"
				redirect_to controller: :usuarios, action: :perfil, id: params[:id]
			end
			@notificaciones = Notificacion.where(usuario_id:session[:id],notificacion_vista:0)
			@lista_deseos = ListaDeseos.where(usuario_id:params[:id]).paginate(page: params[:page], per_page: 4)
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def eliminar_libro_lista
		if session[:id]!=nil
			ListaDeseos.where(libro_id:[params[:libro_id],usuario_id:session[:id]]).take.destroy
			flash[:mensaje] = "El libro ha sido eliminado de su lista de deseos exitosamente"
			redirect_to controller: :lista_de_deseos , action: :lista , id: session[:id]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end


  	def crear_notificacion(tipo_notificacion,usuario_id,descripcion,libro_id,amigo_id)
  		if session[:id]!=nil
	  		if tipo_notificacion == 'LIST-DES'
				@usuario = Usuario.find(usuario_id)
			    if @usuario.tipo_configuracion_id=='COR' || @usuario.tipo_configuracion_id=='NOTyCOR'
			      UserMailer.enviar_notificacion(@usuario,descripcion,tipo_notificacion).deliver_later
			    end
			    notif= Notificacion.new
			    notif.usuario_id= usuario_id
			    notif.descripcion= descripcion
			    notif.tipo_notificacion_id= tipo_notificacion
			    notif.libro_id = libro_id
			    notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
			    notif.amigo_id= amigo_id
			    notif.save
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end
end
