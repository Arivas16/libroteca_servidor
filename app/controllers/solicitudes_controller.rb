class SolicitudesController < ApplicationController
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true
	
	def solicitudes_libros_recibidas
		if session[:id]!=nil
			@solicitudes= Solicitud.where(usuario_dueno_id: session[:id], estado_solicitud_id: 'ENVIADA')
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end		

	def solicitudes_libros_enviadas
		if session[:id]!=nil
			@solicitudes= Solicitud.where(usuario_solicitante_id: session[:id], estado_solicitud_id: 'ENVIADA')
			@usuariosol= []
			@solicitudes.each do |sol|
				@usuariosol.push(Usuario.find(sol.usuario_dueno_id))
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def solicitudes_libros_confirmar
		if session[:id]!=nil
			@solicitudes= Solicitud.where(usuario_solicitante_id: session[:id], estado_solicitud_id: 'ACEPTADA')
			@usuariores= []
			@solicitudes.each do |sol|
				@usuariores.push(Usuario.find(sol.usuario_dueno_id))
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def envio_exitoso
		if session[:id]!=nil
			sol= Solicitud.find([session[:id],params[:dueno_id],params[:libro_id]])
			sol.estado_solicitud_id='REALIZADA'
			usuario= Usuario.find(params[:dueno_id])
			solicitante= Usuario.find(session[:id])
			if sol.tipo_modalidad_id == 'INT'
				usuario.puntuacion= usuario.puntuacion + 7
				solicitante.puntuacion= solicitante.puntuacion + 4
			else
				usuario.puntuacion= usuario.puntuacion + 5
				solicitante.puntuacion= solicitante.puntuacion + 3
			end
			if sol.save && usuario.save && solicitante.save
				flash[:mensaje]= 'Gracias por usar libroteca y haber negociaciado un libro exitosamente'
				mostrar_noticia('ENT',params[:libro_id])
			else
				flash[:mensaje_error]='Ocurrió un error, por favor, inténtelo nuevamente'
				redirect_to controller: :solicitudes, action: :solicitudes_libros_confirmar
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def envio_no_exitoso
		if session[:id]
			sol= Solicitud.find([session[:id],params[:dueno_id],params[:libro_id]])
			sol.estado_solicitud_id='N-REALIZADA'
			usuario= Usuario.find(params[:dueno_id])
			usuario.puntuacion= usuario.puntuacion - 5
			if sol.save && usuario.save
				flash[:mensaje]= 'El envío no exitoso del libro, fue reportado correctamente y el usuario responsable, fue penalizado'
			else
				flash[:mensaje_error]='Ocurrió un error, por favor, inténtelo nuevamente'
			end
			redirect_to controller: :solicitudes, action: :solicitudes_libros_confirmar
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def aceptar_solicitud
		if session[:id]!=nil
			sol= Solicitud.find([params[:solicitante_id],session[:id],params[:libro_id]])
			sol.estado_solicitud_id= 'ACEPTADA'
			descripcion="El usuario "+session[:nombre]+" aceptó tu petición del libro "+sol.libro.titulo+" en la modalidad de "+sol.libro_usuario.tipo_modalidad.descripcion
			if crear_notificacion('P-ACEPTADA',params[:solicitante_id],descripcion,params[:libro_id],session[:id])
				if sol.save
					otrasol= Solicitud.where(usuario_dueno_id: session[:id],libro_id: params[:libro_id], estado_solicitud_id: 'ENVIADA') #Se buscan otras solicitudes que hayan hechas para ese mismo libro y rechazarlas
					
					otrasol.each do |sol|
						descripcion="El usuario "+session[:nombre]+" rechazó tu petición del libro "+sol.libro.titulo+" en la modalidad de "+sol.libro_usuario.tipo_modalidad.descripcion
						crear_notificacion('P-RECHAZADA',sol.usuario_solicitante_id,descripcion,sol.libro_id,session[:id])
						sol.destroy
					end
					
					librou= LibroUsuario.find([session[:id],params[:libro_id]])
					if librou.destroy #Esto es para que no aparezca en las listas de libros y no pueda ser solicitado por nadie mas poque ya se acepto una solicitud y asi evitamos errores de que no soliciten un libro que ya fue negociado
						flash[:mensaje]= "La solicitud del libro ha sido aceptada exitosamente"
						UserMailer.aceptar_solicitud(Usuario.find(params[:solicitante_id]),Usuario.find(session[:id]),Libro.find(params[:libro_id])).deliver_later
						UserMailer.aceptar_solicitud2(Usuario.find(params[:solicitante_id]),Usuario.find(session[:id]),Libro.find(params[:libro_id])).deliver_later
					else
						flash[:mensaje_error]= "Ocurrió un error al aceptar la solicitud del libro, por favor, inténtelo nuevamente"
					end
				else
					flash[:mensaje_error]= "Ocurrió un error al aceptar la solicitud del libro, por favor, inténtelo nuevamente"
				end
			else
				flash[:mensaje_error]= "Ocurrió un error al aceptar la solicitud del libro, por favor, inténtelo nuevamente"
			end
			redirect_to controller: :solicitudes, action: :solicitudes_libros_recibidas
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def rechazar_solicitud
		if session[:id]!=nil
			sol= Solicitud.find([params[:solicitante_id],session[:id],params[:libro_id]])
			descripcion="El usuario "+session[:nombre]+" rechazó tu petición del libro "+sol.libro.titulo+" en la modalidad de "+sol.libro_usuario.tipo_modalidad.descripcion
			if crear_notificacion('P-RECHAZADA',params[:solicitante_id],descripcion,params[:libro_id],session[:id])
				if sol.destroy
					flash[:mensaje]= "La solicitud del libro ha sido rechazada exitosamente"
				else
					flash[:mensaje_error]= "Ocurrió un error al rechazar la solicitud del libro, por favor, inténtelo nuevamente"
				end
			else
				flash[:mensaje_error]= "Ocurrió un error al rechazar la solicitud del libro, por favor, inténtelo nuevamente"
			end
			redirect_to controller: :solicitudes, action: :solicitudes_libros_recibidas
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def cancelar_solicitud
		if session[:id]!=nil
			sol= Solicitud.find([session[:id],params[:dueno_id],params[:libro_id]])
			if sol.destroy
		       flash[:mensaje] = "Usted canceló la solicitud del libro exitosamente"
		    else
		      flash[:mensaje_error] = "Ocurrió un error al cancelar la solicitud, por favor, inténtelo nuevamente"
		    end
		    redirect_to controller: :solicitudes, action: :solicitudes_libros_enviadas
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def crear_notificacion(tipo_notificacion,usuario_id,descripcion,libro_id,amigo_id)
		if session[:id]!=nil
			usuario= Usuario.find(usuario_id)
			if usuario.tipo_configuracion_id=='COR' || usuario.tipo_configuracion_id=='NOTyCOR'
				UserMailer.enviar_notificacion(usuario,descripcion,tipo_notificacion).deliver_later
			end
			notif= Notificacion.new
			notif.usuario_id= usuario_id
			notif.descripcion= descripcion
			notif.tipo_notificacion_id= tipo_notificacion
			notif.libro_id = libro_id
			notif.amigo_id= amigo_id
			notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
			if notif.save
				flash[:mensaje_notificacion]="La notificación ha sido enviada exitosamente"
				return true
			else
				flash[:mensaje_notificacion]="Ocurrió un error al enviar la notificación, por favor, inténtelo nuevamente"
				return false
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def mostrar_noticia(tipo_noticia,id)
		if session[:id]!=nil
		    flash[:mensaje_noticia] = true
		    @tipo_noticia = TipoNoticia.find(tipo_noticia)
		    case tipo_noticia
		      when 'ENT'
		        @texto= Libro.find(id)
		        @noticia= @tipo_noticia.descripcion+@texto.titulo
		        redirect_to controller: :solicitudes, action: :solicitudes_libros_confirmar, noticia: @noticia ,tipo_noticia_id: @tipo_noticia.id ,generador_noticia_id: id
		      else
		        puts "Ocurrió un error"
		    end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	 end

end

