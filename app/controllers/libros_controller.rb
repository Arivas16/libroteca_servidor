class LibrosController < ApplicationController
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true
	

	def form
		if session[:id]!=nil
			@categorias= Categoria.all
		    @subcategoriasEscolar = Subcategoria.where(categoria_id:1)
		    @subcategoriasUniversitario = Subcategoria.where(categoria_id:2)
			@modalidades= TipoModalidad.all
			@condiciones= TipoCondicion.all
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def buscar_isbn
		if session[:id]!=nil
			@libro= Libro.where(isbn:params[:isbn]).take
			@lista_deseos = 0
			if @libro
				@autor= Autor.where(libro_id: @libro.id).take
				@lista_deseos = 1
			end	
			@isbn= params[:isbn]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end


	def mislibros
		if session[:id]!=nil
			@usuario_libro = Usuario.find(params[:id])
			@libros= LibroUsuario.where(usuario_id: params[:id]).all.paginate(:page => params[:page], :per_page => 8)
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def edit_libro
		if session[:id]!=nil
			if params[:libro_id]
				@libro = LibroUsuario.find([session[:id],params[:libro_id]])
				@autores= Autor.where(libro_id: @libro.libro_id).all
				@modalidades= TipoModalidad.all
				@condiciones= TipoCondicion.all
				@modal= @libro.tipo_modalidad_id
			else
				flash[:mensaje]= "Ocurrió un error al editar su libro, por favor, inténtelo nuevamente"
				redirect_to controller: :libros, action: :mislibros , id:session[:id]
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def editar_libro
		if session[:id]!=nil
			if params[:id] 
				@venta= false
				@campo = params[:nombre_del_campo]
	    		@identificador = params[:id]
				librou = LibroUsuario.find([session[:id],params[:id]])
				if params[:modalidad] 
					if params[:modalidad]!='VEN' && librou.tipo_modalidad_id== 'VEN'
						librou.precio=0.0
					end
	  				librou.tipo_modalidad_id= params[:modalidad]
	  				@texto = TipoModalidad.find(params[:modalidad]).descripcion
	  				if params[:modalidad] == 'VEN'
	  					@venta= true
	  				end
	  			end

	  			if params[:condicion]
			   		librou.tipo_condicion_id= params[:condicion]
			   		@texto = TipoCondicion.find(params[:condicion]).descripcion
			   	end

			   	if params[:precio]
			   		librou.precio= params[:precio]
			   		@texto = params[:precio]
			   	end

				if librou.save
					tipo_noticia = TipoNoticia.find('EDITL')
			       	@noticia = tipo_noticia.descripcion
			        @tipo_noticia_id= tipo_noticia.id
			        @generador_noticia_id = librou.libro_id
			        @descripcion= session[:nombre]+" modificó su libro "+librou.libro.titulo
					flash[:mensaje]= "Los datos del libro fueron editados exitosamente"
				else
					flash[:mensaje]= "Ocurrió un error al editar su libro, por favor, inténtelo nuevamente"
				end
			else
				flash[:mensaje]= "Ocurrió un error al editar su libro, por favor, inténtelo nuevamente"
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def eliminar_libro
		if session[:id]!=nil
			if params[:libro_id]
				librou = LibroUsuario.find([session[:id],params[:libro_id]])
				if librou.destroy
					flash[:mensaje]= "El libro ha sido eliminado exitosamente"
				else
					flash[:mensaje]= "Ocurrió un error al eliminar su libro, por favor, inténtelo nuevamente"
				end
			else
				flash[:mensaje]= "Ocurrió un error al eliminar su libro, por favor, inténtelo nuevamente"
			end
			redirect_to controller: :libros, action: :mislibros, id:session[:id]
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def crear_libro
		if session[:id]!=nil
			@libro= Libro.where(isbn: params[:isbn2]).take
			id_libro=nil
			if @libro==nil #El libro no existe en la BD y hay que crearlo
				libron= Libro.new

				libron.isbn= params[:isbn2]
				libron.titulo= params[:titulo].capitalize
				libron.ano= params[:ano]
				libron.editorial= params[:editorial].capitalize
				libron.puntuacion=0
				libron.cantidad_votos=0
				libron.categoria_id= params[:categoria]
				if params[:categoria].to_i==1
					libron.grado= params[:grado]
				end
				libron.lista_deseo=0

				if libron.save
					id_libro= libron.id
				end
				#Ahora se guardan los autores
				if id_libro!=nil
		  			autor= Autor.new
		  			autor.nombre= params[:autor].capitalize
		  			autor.libro_id= id_libro
		  			autor.save
		  			cant_autores= params[:cant_autor].to_i
		  			j=0
					i=:nuevoaut.to_s+j.to_s
					nautor = params[i]
					while j < cant_autores
		      			if nautor!=nil && nautor!=""
							aut= Autor.new
							aut.nombre= nautor
							aut.libro_id= id_libro
							aut.save
					 	end
		      			j = j + 1
		      			i=:nuevoaut.to_s+j.to_s;
		      			nautor = params[i]
				  	end
			 	end
			else
				if @libro.lista_deseo==true
					id_libro= @libro.id

					@libro.titulo= params[:titulo].capitalize
					@libro.ano= params[:ano]
					@libro.editorial= params[:editorial].capitalize
					@libro.puntuacion=0
					@libro.cantidad_votos=0
					@libro.categoria_id= params[:categoria]
					if params[:categoria].to_i==1
						@libro.grado= params[:grado]
					end
		  			autor= Autor.new
		  			autor.nombre= params[:autor].capitalize
		  			autor.libro_id= id_libro
		  			autor.save
		  			cant_autores= params[:cant_autor].to_i
		  			j=0
					i=:nuevoaut.to_s+j.to_s
					nautor = params[i]
					while j < cant_autores
		      			if nautor!=nil && nautor!=""
							aut= Autor.new
							aut.nombre= nautor
							aut.libro_id= id_libro
							aut.save
					 	end
		      			j = j + 1
		      			i=:nuevoaut.to_s+j.to_s;
		      			nautor = params[i]
				  	end
				  	@libro.save
				end
				id_libro= @libro.id
			end

			#busco a los usuarios que desean este libro
			@usuarios= ListaDeseos.where(libro_id: id_libro)
			@lib= Libro.find(id_libro)
			crear_notificacion('LIST-DES',@usuarios,"El usuario "+Usuario.find(session[:id]).nombre+" agregó el libro que deseabas: "+@lib.titulo,@lib.id,session[:id])


			if id_libro!=nil
				lu= LibroUsuario.where(usuario_id: session[:id],libro_id: id_libro).take
				if lu==nil
					librou= LibroUsuario.new
					tiempo = Time.now
  					librou.fecha_creacion= tiempo.strftime("%Y-%m-%d").to_s
					librou.usuario_id= session[:id]
					librou.libro_id= id_libro
					librou.tipo_modalidad_id= params[:modalidad]
					if params[:precio]
						puts params[:precio]
						librou.precio= params[:precio]
					end
					librou.tipo_condicion_id= params[:condicion]
					librou.categoria_id = params[:categoria]
					if params[:categoria].to_i == 1
						librou.subcategoria_id = params[:subcategoriaEscolar]
					else
						librou.subcategoria_id = params[:subcategoriaUniversitario]
					end
			  		if librou.save
						flash[:mensaje]= "El libro ha sido guardado exitosamente"
						usuario= Usuario.find(session[:id])
						if librou.tipo_modalidad_id == 'INT'
							usuario.puntuacion= usuario.puntuacion + 3
						else
							usuario.puntuacion= usuario.puntuacion + 2
						end
						usuario.save
						mostrar_noticia("LIB",id_libro)
			  		else
			    		flash[:mensaje_error]= "Ocurrió un error al guardar su libro, por favor, inténtelo nuevamente"
			  			redirect_to controller: :libros, action: :mislibros , id:session[:id]
			  		end
			  	else
			  		flash[:mensaje_error]= "Usted ya tiene agregado este libro, si desea editar su información, elija en el libro la opción de editar"
			  		redirect_to controller: :libros, action: :mislibros , id:session[:id]
			  	end
			else
				flash[:mensaje_error]= "Ocurrió un error al guardar su libro, por favor, inténtelo nuevamente"
				redirect_to controller: :libros, action: :mislibros , id:session[:id]
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

 	def agregar_subcategoria
 		if session[:id]!=nil
		    @subcategoria = Subcategoria.where(descripcion:params[:subcategoria].capitalize,categoria_id:params[:categoria]).take
		    @error = ""
		    if @subcategoria
		      @error = true
		      @nuevo = @subcategoria
		    else
		      @error = false
		      @nuevo = Subcategoria.create(descripcion:params[:subcategoria].capitalize, categoria_id:params[:categoria])
		    end
		    @categoria = params[:categoria].to_s
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def listar_libros_completos
		if session[:id]!=nil
			if params[:subcategoria_id]
				if params[:subcategoria_id] == "todos"
					@libros_todos = LibroUsuario.where(categoria_id:params[:id]).paginate(:page => params[:page], :per_page => 16)
				else
					@libros_todos = LibroUsuario.where(categoria_id:params[:id],subcategoria_id:params[:subcategoria_id]).paginate(:page => params[:page], :per_page => 16)
				end
			else
				@libros_todos = LibroUsuario.where(categoria_id:params[:id]).paginate(:page => params[:page], :per_page => 16)
			end
			@categoria = params[:id]
			@subcategoria = Subcategoria.where(categoria_id:params[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def listar_libros_compra
		if session[:id]!=nil
			if params[:subcategoria_id]
				if params[:subcategoria_id] == "todos"
					@libros_compra = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"VEN").paginate(:page => params[:page], :per_page => 16)
				else
					@libros_compra = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"VEN",subcategoria_id:params[:subcategoria_id]).paginate(:page => params[:page], :per_page => 16)
				end
				
			else
				@libros_compra = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"VEN").paginate(:page => params[:page], :per_page => 16)
			end
			@categoria = params[:id]
			@subcategoria = Subcategoria.where(categoria_id:params[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def listar_libros_intercambio
		if session[:id]!=nil
			if params[:subcategoria_id]
				if params[:subcategoria_id] == "todos"
					@libros_intercambio = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"INT").paginate(:page => params[:page], :per_page => 16)
				else
					@libros_intercambio = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"INT",subcategoria_id:params[:subcategoria_id]).paginate(:page => params[:page], :per_page => 16)
				end
				
			else
				@libros_intercambio = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"INT").paginate(:page => params[:page], :per_page => 16)
			end
			@categoria = params[:id]
			@subcategoria = Subcategoria.where(categoria_id:params[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def listar_libros_donacion
		if session[:id]!=nil
			if params[:subcategoria_id]
				if params[:subcategoria_id] == "todos"
					@libros_donacion = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"DON").paginate(:page => params[:page], :per_page => 16)
				else
					@libros_donacion = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"DON",subcategoria_id:params[:subcategoria_id]).paginate(:page => params[:page], :per_page => 16)
				end			
			else
				@libros_donacion = LibroUsuario.where(categoria_id:params[:id],tipo_modalidad_id:"DON").paginate(:page => params[:page], :per_page => 16)
			end
			@categoria = params[:id]
			@subcategoria = Subcategoria.where(categoria_id:params[:id])
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def ver_libro
		if session[:id]!=nil
			@libro = LibroUsuario.find([params[:usuario_id],params[:id]])
		else
			flash[:mensaje_error]="Para ver los detalles de este libro, por favor, regístrese en la aplicación"
			redirect_to controller: :bienvenida, action: :index
		end
	end

	def solicitar_libro
		@libro_usuario = LibroUsuario.find([params[:usuario_id],params[:libro_id]])
		tiempo = Time.now
		@solicitar= true
		deseo= false
		if session[:id]!=nil
			if @libro_usuario.tipo_modalidad_id == "INT"
				@libros_propios = LibroUsuario.where(usuario_id:session[:id],tipo_modalidad_id:"INT")
				@solicitudes_exitosasenv = Solicitud.where(usuario_solicitante_id:session[:id],tipo_modalidad_id:"INT", estado_solicitud_id:["ACEPTADA", "REALIZADA", "ENVIADA"])
				@solicitudes_exitosasrec = Solicitud.where(usuario_dueno_id:session[:id],tipo_modalidad_id:"INT",estado_solicitud_id:"REALIZADA")
				@result= @solicitudes_exitosasrec.count+@libros_propios.count
				if @solicitudes_exitosasenv.count >=  @result
						@solicitar= false
						flash[:mensaje_error] = "Debe agregar un libro en la modalidad intercambio para poder realizar esta solicitud"
						redirect_to controller: :escritorio , action: :inicio
				end
			end
			if @solicitar
				@lista_deseo= ListaDeseos.where(libro_id:params[:libro_id], usuario_id: session[:id]).take
				if @lista_deseo
					@lista_deseo.destroy
					deseo=true
				end
				if deseo
					flash[:mensaje]="La solicitud ha sido enviada exitosamente y el libro ha sido borrado de su lista de deseos"
				else
					flash[:mensaje]="La solicitud ha sido enviada exitosamente"
				end
				Solicitud.create(usuario_solicitante_id:session[:id],usuario_dueno_id:params[:usuario_id],libro_id:params[:libro_id],fecha_solicitud:tiempo.strftime("%Y-%m-%d").to_s,tipo_modalidad_id:@libro_usuario.tipo_modalidad_id,estado_solicitud_id:"ENVIADA")
				@creo_notificacion = crear_notificacion("SOLICITUDL",params[:usuario_id],"El usuario "+Usuario.find(session[:id]).nombre+" solicitó tu libro "+Libro.find(params[:libro_id]).titulo,params[:libro_id],session[:id])
				redirect_to controller: :solicitudes , action: :solicitudes_libros_enviadas
			end
		else
			flash[:mensaje_error]="Para solicitar este libro, por favor, regístrese en la aplicación"
			redirect_to controller: :bienvenida, action: :index
		end
	end


  	def mostrar_noticia(tipo_noticia,id)
  		if session[:id]!=nil
		  	flash[:mensaje_noticia] = true
		  	@tipo_noticia = TipoNoticia.find(tipo_noticia)
		  	case tipo_noticia
				when "LIB"
					@texto = LibroUsuario.where(libro_id:id, usuario_id:session[:id]).take
					@noticia = @tipo_noticia.descripcion+" "+@texto.libro.titulo
					redirect_to controller: :libros, action: :mislibros , id:session[:id], noticia: @noticia ,tipo_noticia_id: @tipo_noticia.id ,generador_noticia_id: id
				when "EDITL"
					@texto = LibroUsuario.find([session[:id],id])
					@noticia = @tipo_noticia.descripcion
					#redirect a la vista que deberia verse luego de editar un libro
				else
				  puts "Ocurrió un error"
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
  	end

  	def busqueda_libros
  		if session[:id]!=nil
  			@lista=[]
			@modalidades=TipoModalidad.all
			@categorias= Categoria.all
		    @subcategoriasEscolar = Subcategoria.where(categoria_id:1)
		    @subcategoriasUniversitario = Subcategoria.where(categoria_id:2)
			if params[:buscar]!=nil
				@palabra=params[:buscar].downcase
				@libros= Libro.where("titulo LIKE ?", "%#{@palabra}%").order("titulo ASC") # se consulta a la base de datos ordenando de forma ascendente	
				if @libros!=nil
					@libros.each do |lib|
						@librou= LibroUsuario.where(libro_id: lib.id).all
						@librou.each do |l|
							@lista.push(l)
						end
					end
					puts @lista.size()
					@pages= @lista.paginate(:page => params[:page], :per_page => 8)
				end
			else
				@lista=nil
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

	def busqueda_general
		if session[:id]!=nil
			@modalidades=TipoModalidad.all
			@categorias= Categoria.all
		    @subcategoriasEscolar = Subcategoria.where(categoria_id:1)
		    @subcategoriasUniversitario = Subcategoria.where(categoria_id:2)
			@libros=[]
			if params[:titulo]!=nil
				palabra=params[:titulo].downcase
				libs= Libro.where("titulo LIKE ?", "%#{palabra}%").order("titulo ASC") # se consulta a la base de datos ordenando de forma ascendente	
				if libs!=nil
					libs.each do |lib|
						librou= LibroUsuario.where(libro_id: lib.id).all
						librou.each do |l|
							@libros.push(l)
						end
					end
				end
			end
			if params[:isbn]
				palabra= params[:isbn].downcase
				libros= Libro.where("isbn LIKE ?", "%#{palabra}%")
				if libros
					libros.each do |lib|
						isbns= LibroUsuario.where(libro_id: lib.id)
						isbns.each do |l|
							@libros.push(l)
						end
					end
				end
			end
			if params[:autor]
				palabra= params[:autor].downcase
				autores= Autor.where("nombre LIKE ?", "%#{palabra}%")
				if autores
					autores.each do |au|
						libs=LibroUsuario.where(libro_id: au.libro_id)
						libs.each do |lib|
							@libros.push(lib)
						end
					end
				end
			end
			if params[:editorial]
				palabra= params[:editorial].downcase
				libs= Libro.where("editorial LIKE ?", "%#{palabra}%")
				if libs
					libs.each do |lib|
						libs2=LibroUsuario.where(libro_id: lib.id)
						libs2.each do |lib|
							@libros.push(lib)
						end
					end
				end
			end

			if params[:modalidad]
				libs= LibroUsuario.where(tipo_modalidad_id: params[:modalidad])
				libs.each do |lib|
					@libros.push(lib)
				end
			end

			if params[:categoria]
				if params[:categoria].to_i == 1
					libs= LibroUsuario.where(categoria_id: params[:categoria],subcategoria_id: params[:subcategoriaEscolar])
				else
					libs= LibroUsuario.where(categoria_id: params[:categoria],subcategoria_id: params[:subcategoriaUniversitario])
				end
				libs.each do |lib|
					@libros.push(lib)
				end
			end

			@pages= @libros.paginate(:page => params[:page], :per_page => 8)
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end


  	def crear_notificacion(tipo_notificacion,usuario_id,descripcion,libro_id,amigo_id)
  		if session[:id]!=nil
	  		if tipo_notificacion == 'LIST-DES'
	  			usuario_id.each do |u|
					@usuario = Usuario.find(u.usuario_id)
				    if @usuario.tipo_configuracion_id=='COR' || @usuario.tipo_configuracion_id=='NOTyCOR'
				      UserMailer.enviar_notificacion(@usuario,descripcion,tipo_notificacion).deliver_later
				    end
				    notif= Notificacion.new
				    notif.usuario_id= u.usuario_id
				    notif.descripcion= descripcion
				    notif.tipo_notificacion_id= tipo_notificacion
				    notif.libro_id = libro_id
				    notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
				    notif.amigo_id= amigo_id
				    notif.save
				end
	  		else
		  		usuario= Usuario.find(usuario_id)
			    if usuario.tipo_configuracion_id=='COR' || usuario.tipo_configuracion_id=='NOTyCOR'
			      UserMailer.enviar_notificacion(usuario,descripcion,tipo_notificacion).deliver_later
			    end
			    notif= Notificacion.new
			    notif.usuario_id= usuario_id
			    notif.descripcion= descripcion
			    notif.tipo_notificacion_id= tipo_notificacion
			    notif.libro_id = libro_id
			    notif.amigo_id= amigo_id
			    notif.fecha_creacion= Time.now.strftime("%Y-%m-%d")
			    if notif.save
			      flash[:mensaje_notificacion]="La notificación ha sido enviada exitosamente"
			      return true
			    else
			      flash[:mensaje_notificacion]="Ocurrió un error, por favor, inténtelo nuevamente"
			      return false
			    end
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

end

