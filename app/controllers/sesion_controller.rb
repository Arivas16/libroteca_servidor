class SesionController < ActionController::Base

	def cerrar_sesion
	    reset_session
	    flash[:mensaje]= nil
	    flash[:mensaje_error]= nil
	    redirect_to controller: :bienvenida, action: :index
	    return
  	end

	def validar_sesion
		reset_session
		usuario = Usuario.where(email: params[:email]).take
		contrasena = Usuario.md5(params[:clave])
		@error = false

		if usuario && contrasena == usuario.clave
			session[:logueado] = true
			session[:id] = usuario.id
			session[:nombre] = usuario.nombre
		else
			@error =  true
			return
		end
	end

	def recordar_contrasena
		usuario= Usuario.where(email: params[:email]).take

		if usuario
			clave= Usuario.random_alphanumeric
			usuario.clave= Usuario.md5(clave)
			usuario.save
			UserMailer.olvido_clave(usuario,clave).deliver_later
          	#flash[:mensaje]= "Su clave fue reestablecida satisfactoriamente, por favor inicie sesión"
			puts "recordó la contraseña"
			@error = false
		else
			@error = true
			#flash[:mensaje_error] = "El email que introdujo es incorrecto, por favor verifique"
			puts "no recordó la contraseña"	
		end
		
	end

end

