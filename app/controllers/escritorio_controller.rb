require 'will_paginate/array'
class EscritorioController < ActionController::Base
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true
	
	def inicio
        if session[:id]!=nil 
    		@notificaciones = Notificacion.where(usuario_id:session[:id],notificacion_vista:0)
    		@libros = LibroUsuario.limit(10).order(fecha_creacion: :desc)
    		@amigos = Usuario.amigos_id(session[:id])
    		@noticias_amigos = Array.new
        	@amigos.each do |a|
        		noti = Noticia.where(usuario_id:a).order(fecha_creacion: :desc)
        		if !noti.empty?
        			@noticias_amigos.push(noti)
        		end
        	end
        	@n = Array.new
        	cont = 0
        	sigo = true
        	while sigo do
        		sigo = false
        		@noticias_amigos.each do |x|
    	    		if x[cont] != nil
    	    			@n.push(x[cont])
    	    			sigo = true
    	    		end
        		end
        		cont = cont +1
        	end
        	
        	@noticias = @n.paginate(:page => params[:page], :per_page => 4)
        else
            flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
        end
	end

    def informacion
    end
end
