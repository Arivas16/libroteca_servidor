class NotificacionesController < ActionController::Base
	layout 'interno'
	autocomplete :libro, :isbn
	autocomplete :libro, :titulo, :full => true
	autocomplete :autor, :nombre, :full => true
	autocomplete :libro, :editorial, :full => true

	def ver_notificaciones
		if session[:id]!=nil
			@notificaciones_vistas = Notificacion.where(usuario_id:session[:id]).order(fecha_creacion: :desc)
			@notificaciones_no_vistas = Notificacion.where(usuario_id:session[:id],notificacion_vista:0).order(fecha_creacion: :desc)
			@notificaciones_no_vistas.each do |n|
				n.notificacion_vista = 1
				n.save
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
            redirect_to controller: :bienvenida, action: :index
		end
	end

end
