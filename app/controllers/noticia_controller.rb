class NoticiaController < ActionController::Base
	
	def guardar_noticia
		if session[:id]!=nil
			@descripcion = TipoNoticia.find(params[:tipo_noticia_id])
			tiempo = Time.now
			case params[:tipo_noticia_id]
				when "AMIG"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s,usuario_id:session[:id],descripcion:@descripcion.descripcion,libro_id:nil,grupo_lectura_id:nil, tipo_noticia_id:params[:tipo_noticia_id], amigo_id: params[:generador_noticia_id])
				when "EST"
					Noticia.create()
				when "ENT"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s ,usuario_id:session[:id],descripcion:@descripcion.descripcion, libro_id:params[:generador_noticia_id],grupo_lectura_id:nil,tipo_noticia_id:params[:tipo_noticia_id], amigo_id: nil)
				when "LIB"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s ,usuario_id:session[:id],descripcion:@descripcion.descripcion, libro_id:params[:generador_noticia_id],grupo_lectura_id:nil,tipo_noticia_id:params[:tipo_noticia_id], amigo_id: nil)
				when "GRUP"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s ,usuario_id:session[:id],descripcion:@descripcion.descripcion, tipo_noticia_id:params[:tipo_noticia_id], grupo_lectura_id:params[:generador_noticia_id])
				when "PERF"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s ,usuario_id:session[:id],descripcion:@descripcion.descripcion, libro_id:nil,grupo_lectura_id:nil,tipo_noticia_id:params[:tipo_noticia_id], amigo_id: nil)
				when "EDITL"
					Noticia.create(fecha_creacion: tiempo.strftime("%Y-%m-%d").to_s,usuario_id:session[:id],descripcion:@descripcion.descripcion, libro_id:params[:generador_noticia_id],grupo_lectura_id:nil,tipo_noticia_id:params[:tipo_noticia_id], amigo_id: nil)
				else
				  puts "Ocurrio un error"
			end
		else
			flash[:mensaje_error]="Por favor, inicie sesión"
      		redirect_to controller: :bienvenida, action: :index
		end
	end

end
