class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  
   layout 'interno'

	before_action :notificaciones
  def notificaciones
  	#puts "***********************************************"
    @notificaciones = Notificacion.where(usuario_id:session[:id],notificacion_vista:0)
  end

end
