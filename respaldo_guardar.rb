def crear_libro
		@libro= Libro.where(isbn: params[:isbn2]).take
		id_libro=nil
		puts "***********************************************"
		puts params[:lista_deseos]
		if @libro==nil || params[:lista_deseos].to_i == 1 #El libro no existe en la BD o existe pero fue creado por lista de deseo y hay que llenar los campos
			puts "entre al primer if"
			if @libro==nil # El libro no existe, no ha sido creado por la lista de deseos
				puts "entre al if"
				libron= Libro.new
				libron.isbn= params[:isbn2]
			else # El libro existe pero fue creado por la lista de deseos, no creo una nueva instacia, edito la que ya existe
				puts "entre al else"
				libron = @libro
			end
			libron.titulo= params[:titulo].capitalize
			libron.ano= params[:ano]
			libron.editorial= params[:editorial].capitalize
			libron.puntuacion=0
			libron.cantidad_votos=0
			libron.categoria_id= params[:categoria]
			libron.lista_deseo=0
			if libron.save
				id_libro= libron.id
			end
			#Ahora se guardan los autores
			if id_libro!=nil
	  			autor= Autor.new
	  			autor.nombre= params[:autor].capitalize
	  			autor.libro_id= id_libro
	  			autor.save
	  			cant_autores= params[:cant_autor].to_i
	  			j=0
				i=:nuevoaut.to_s+j.to_s
				nautor = params[i]
				while j < cant_autores
          			if nautor!=nil && nautor!=""
						aut= Autor.new
						aut.nombre= nautor
						aut.libro_id= id_libro
						aut.save
				 	end
          			j = j + 1
          			i=:nuevoaut.to_s+j.to_s;
          			nautor = params[i]
			  	end
		 	end
		else
			puts "entre al segundo else"
			id_libro= @libro.id
		end

		#busco a los usuarios que desean este libro
		@usuarios= ListaDeseos.where(libro_id: id_libro)
		@lib= Libro.find(id_libro)
		crear_notificacion('LIST-DES',@usuarios,"El usuario "+Usuario.find(session[:id]).nombre+" agrego el libro que deseabas: "+@lib.titulo,@lib.id,session[:id])

		if id_libro!=nil
  			librou= LibroUsuario.new
			librou.usuario_id= session[:id]
			librou.libro_id= id_libro
			librou.tipo_modalidad_id= params[:modalidad]
			if params[:precio]
				puts params[:precio]
				librou.precio= params[:precio]
			end
			librou.tipo_condicion_id= params[:condicion]
			librou.categoria_id = params[:categoria]
			if params[:categoria].to_i == 1
				librou.subcategoria_id = params[:subcategoriaEscolar]
			else
				librou.subcategoria_id = params[:subcategoriaUniversitario]
			end
      		if librou.save
				flash[:mensaje]= "El libro ha sido guardado exitosamente"
				usuario= Usuario.find(session[:id])
				if librou.tipo_modalidad_id == 'INT'
					usuario.puntuacion= usuario.puntuacion + 3
				else
					usuario.puntuacion= usuario.puntuacion + 2
				end
				usuario.save
      		else
        		flash[:mensaje]= "Ocurrió un error al guardar su libro, por favor intentelo nuevamente"
      		end
    	else
			flash[:mensaje]= "Ocurrió un error al guardar su libro, por favor intentelo nuevamente"
    	end
    	mostrar_noticia("LIB",id_libro)
	end