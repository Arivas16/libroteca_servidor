Rails.application.routes.draw do
  
  scope "/libroteca" do
    get ':controller(/:action(/:id(.:format)))'
    post ':controller(/:action(/:id(.:format)))'
    root 'bienvenida#index'
  end

  get 'libros/autocomplete_libro_isbn', to:'libros#autocomplete_libro_isbn'
  get 'libros/autocomplete_libro_titulo', to:'libros#autocomplete_libro_titulo'
  get 'libros/autocomplete_libro_editorial', to:'libros#autocomplete_libro_editorial'
  get 'libros/autocomplete_autor_nombre', to:'libros#autocomplete_autor_nombre'

  
  get 'bienvenida/autocomplete_libro_titulo', to:'bienvenida#autocomplete_libro_titulo'
  get 'bienvenida/autocomplete_libro_isbn', to:'bienvenida#autocomplete_libro_isbn'
  get 'bienvenida/autocomplete_libro_editorial', to:'bienvenida#autocomplete_libro_editorial'
  get 'bienvenida/autocomplete_autor_nombre', to:'bienvenida#autocomplete_autor_nombre'
  get 'bienvenida/libros/autocomplete_libro_titulo', to:'bienvenida#autocomplete_libro_titulo'
  get 'lista_de_deseos/autocomplete_libro_titulo', to:'lista_de_deseos#autocomplete_libro_titulo'
  get 'lista_de_deseos/autocomplete_libro_isbn', to:'lista_de_deseos#autocomplete_libro_isbn'
  get 'lista_de_deseos/autocomplete_libro_editorial', to:'lista_de_deseos#autocomplete_libro_editorial'
  get 'lista_de_deseos/autocomplete_autor_nombre', to:'lista_de_deseos#autocomplete_autor_nombre'
  get 'escritorio/autocomplete_libro_titulo', to:'escritorio#autocomplete_libro_titulo'
  get 'escritorio/autocomplete_libro_isbn', to:'escritorio#autocomplete_libro_isbn'
  get 'escritorio/autocomplete_libro_editorial', to:'escritorio#autocomplete_libro_editorial'
  get 'escritorio/autocomplete_autor_nombre', to:'escritorio#autocomplete_autor_nombre'
  get 'grupo_de_lectura/autocomplete_libro_titulo', to:'grupo_de_lectura#autocomplete_libro_titulo'
  get 'grupo_de_lectura/autocomplete_libro_isbn', to:'grupo_de_lectura#autocomplete_libro_isbn'
  get 'grupo_de_lectura/autocomplete_libro_editorial', to:'grupo_de_lectura#autocomplete_libro_editorial'
  get 'grupo_de_lectura/autocomplete_autor_nombre', to:'grupo_de_lectura#autocomplete_autor_nombre'
  get 'solicitudes/autocomplete_libro_titulo', to:'solicitudes#autocomplete_libro_titulo'
  get 'solicitudes/autocomplete_libro_isbn', to:'solicitudes#autocomplete_libro_isbn'
  get 'solicitudes/autocomplete_libro_editorial', to:'solicitudes#autocomplete_libro_editorial'
  get 'solicitudes/autocomplete_autor_nombre', to:'solicitudes#autocomplete_autor_nombre'
  get 'usuarios/autocomplete_libro_titulo', to:'usuarios#autocomplete_libro_titulo'
  get 'usuarios/autocomplete_libro_isbn', to:'usuarios#autocomplete_libro_isbn'
  get 'usuarios/autocomplete_libro_editorial', to:'usuarios#autocomplete_libro_editorial'
  get 'usuarios/autocomplete_autor_nombre', to:'usuarios#autocomplete_autor_nombre'
  get 'notificaciones/autocomplete_libro_titulo', to:'notificaciones#autocomplete_libro_titulo'
  get 'notificaciones/autocomplete_libro_isbn', to:'notificaciones#autocomplete_libro_isbn'
  get 'notificaciones/autocomplete_libro_editorial', to:'notificaciones#autocomplete_libro_editorial'
  get 'notificaciones/autocomplete_autor_nombre', to:'notificaciones#autocomplete_autor_nombre'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
