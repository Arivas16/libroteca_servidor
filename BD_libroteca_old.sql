

CREATE TABLE `tipo_configuracion` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_amigo` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL UNIQUE,
  `telefono` varchar(255) NOT NULL,
  `direccion` longtext DEFAULT NULL,
  `clave` varchar(255) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `intereses` longtext DEFAULT NULL,
  `tipo_configuracion_id` char(30) NOT NULL DEFAULT 1,
  `comunidad_educativa` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `tipoConfiguracion` FOREIGN KEY (`tipo_configuracion_id`) REFERENCES `tipo_configuracion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `subcategoriaCategoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `libro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(255) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `grado` varchar(255) DEFAULT NULL,
  `editorial` varchar(255) DEFAULT NULL,
  `puntuacion` int(11) DEFAULT NULL,
  `cantidad_votos` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `lista_deseo` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `libroCategoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_modalidad` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_condicion` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `libro_usuario` (
  `usuario_id` int(11) NOT NULL,
  `libro_id` int(11) NOT NULL,
  `tipo_modalidad_id` char(30) NOT NULL,
  `tipo_condicion_id` char(30) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `fecha_creacion` date DEFAULT NULL,
  `precio` REAL DEFAULT 0.0,
  PRIMARY KEY (`usuario_id`,`libro_id`),
  CONSTRAINT `usuarioLibro` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `libroUsuario` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modalidadLibro` FOREIGN KEY (`tipo_modalidad_id`) REFERENCES `tipo_modalidad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `condicionLibro` FOREIGN KEY (`tipo_condicion_id`) REFERENCES `tipo_condicion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SubcategoriaLibro` FOREIGN KEY (`subcategoria_id`) REFERENCES `subcategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `libro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `autorLibro` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `lista_deseos` (
  `libro_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`usuario_id`,`libro_id`),
  CONSTRAINT `deseoUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `deseoLibo` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;



CREATE TABLE `grupo_lectura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `grupoLectura` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `grupo_lectura_usuario` (
  `grupo_lectura_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo_amigo_id` char(30) NOT NULL, 
  PRIMARY KEY (`grupo_lectura_id`,`usuario_id`),
  CONSTRAINT `usuarioGrupo` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarioLectura` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `usuarioGrupoLecturaTipo` FOREIGN KEY (`tipo_amigo_id`) REFERENCES `tipo_amigo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `comentario_grupo_lectura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` longtext NOT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `grupo_lectura_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `comentarioGrupo` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comentarioUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `estado_solicitud` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `tipo_notificacion` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `notificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL ,
  `descripcion` varchar(255) NOT NULL ,
  `tipo_notificacion_id` char(30) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `libro_id` int(11) DEFAULT NULL,
  `grupo_lectura_id` int(11) DEFAULT NULL,
  `amigo_id` int (11) DEFAULT  NULL,
  `notificacion_vista` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `notificacionUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LibroNotificacion` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GrupoLecturaNotificacion` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notificacionTipo` FOREIGN KEY (`tipo_notificacion_id`) REFERENCES `tipo_notificacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  )ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `solicitud` (
  `usuario_solicitante_id` int(11) NOT NULL,
  `usuario_dueno_id` int(11) NOT NULL ,
  `libro_id` int(11) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `tipo_modalidad_id` char(30) NOT NULL,
  `estado_solicitud_id` char(30) NOT NULL,
  PRIMARY KEY (`usuario_solicitante_id`,`usuario_dueno_id`,`libro_id`),
  CONSTRAINT `solicitudUsuario_idx` FOREIGN KEY (`usuario_solicitante_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudLibro_idx` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudTipo_idx` FOREIGN KEY (`tipo_modalidad_id`) REFERENCES `tipo_modalidad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudEstado_idx` FOREIGN KEY (`estado_solicitud_id`) REFERENCES `estado_solicitud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_noticia` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `usuario_amigo` (
  `usuario_id` int(11) NOT NULL,
  `amigo_id` int(11) NOT NULL,
  `tipo_amigo_id` char(30) NOT NULL,
  PRIMARY KEY (`usuario_id`,`amigo_id`),
  CONSTRAINT `usuarioAmigo` FOREIGN KEY (`amigo_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarioAmigoTipo` FOREIGN KEY (`tipo_amigo_id`) REFERENCES `tipo_amigo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` date DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `descripcion` varchar(255),
  `libro_id` int(11) DEFAULT NULL,
  `grupo_lectura_id` int(11) DEFAULT NULL,
  `tipo_noticia_id` char(30) NOT NULL,
  `amigo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `UsuarioNoticia` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LibroNoticia` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GrupoLecturaNoticia` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipoNoticiaUsuario` FOREIGN KEY (`tipo_noticia_id`) REFERENCES `tipo_noticia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


INSERT INTO `tipo_configuracion` VALUES ('NOT','Solo por la aplicación'),
('COR','Solo al correo'),
('NOTyCOR','Por la aplicación y el correo');

INSERT INTO `tipo_modalidad` VALUES ('INT','Intercambio'),
('VEN','Venta'),
('DON','Donacion');

INSERT INTO `tipo_condicion` VALUES ('NUEV','Como nuevo'),
('BUE','Buena'),
('MED','Intermedia'),
('MAL','Mala');

INSERT INTO `tipo_noticia` VALUES ('AMIG','es amigo de '),
('EST','publico un estado'),
('ENT','concreto exitosamente una negociación de un libro '),
('LIB','agrego el libro:'),
('GRUP','creo el grupo de lectura'),
('PERF','modifico su perfil'),
('EDITL','edito la informacion del libro: ');


INSERT INTO `tipo_amigo` VALUES ('ACEP','Acepto solicitud'),
('RECH','Rechazo solicitud'),
('NOR','No ha respondido');

INSERT INTO `tipo_notificacion` VALUES ('AMIGO','un amigo acepto la solicitud de amistad'),
  ('A-RECHAZADA','invitación de amistad rechazada'),
  ('COMENTARIO-L','comentaron un grupo de lectura'),
  ('P-ACEPTADA','aceptaron peticion de libro'),
  ('P-RECHAZADA','rechazaron peticion de libro'),
  ('SOLICITUDL','solicitaron un libro'),
  ('COMENTARIO-E','comentaron tu estado'),
  ('INVT-L','invitación a grupo de lectura'),
  ('LIST-DES','libro deseado disponible'),
  ('INVT-A','invitación de amistad recibida'),
  ('ELI-G', 'elimino el grupo de lectura'),
  ('ELI-G-M', 'te elimino del grupo de lectura'),
  ('G-RECHAZADA', 'rechazo la invitación al grupo de lectura'),
  ('G-EDITO','edito el grupo de lectura'),
  ('G-ACEPTADA','acepto la invitación al grupo de lectura');


INSERT INTO `estado_solicitud` VALUES ('REALIZADA','Realizada exitosamente'),
('ENVIADA','Solicitud enviada y no aceptada aun'),
('N-REALIZADA','Realizada pero no fue concluida exitosamente'),
('ACEPTADA','Solicitud aceptada esperando envio');



INSERT INTO `categoria` VALUES (1,'Escolar'),
(2,'Univesitario');



INSERT INTO `subcategoria` (`id`, `descripcion`, `categoria_id`) VALUES
(25, 'Arte', 2),
(26, 'Lenguaje', 1),
(27, 'Literatura', 1),
(28, 'Ciencia', 1),
(29, 'Matematica', 2),
(30, 'Computación', 2),
(31, 'Biología', 2),
(32, 'Comunicación social', 2);





INSERT INTO `usuario` VALUES (1,'sherezada moubayyed','mariet.moubayyed@gmail.com','04342132','Edo. vargas, macuto','827ccb0eea8a706c4c34a16891f84e7b',0,'kaskkskss','NOT','ceapucv'),(2,'Heyiner Espina','heyi@gmail.com','04342132','Edo. vargas, macuto','827ccb0eea8a706c4c34a16891f84e7b',0,'kaskkskss','NOT','ceapucv'),(25,'Zeus','zeus@gmail.com','02128938393','carcas','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','NOT','ceapucv'),(26,'Jorge moubayyed','jorge@gmail.com','02128272827','casa','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv'),(27,'Hector navarro','hector.navarro@ciens.ucv.ve','04168461434','Caracas','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv'),(28,'Esmitt ramirez','esmitt.ramirez@gmail.com','04161083339','sta rosalia','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv'),(29,'Annerys rivas','annerys.rivas@gmail.com','04123735584','','e10adc3949ba59abbe56e057f20f883e',7,'-inglés-computación','NOTyCOR','ciencias');

