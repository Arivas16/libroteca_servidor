


CREATE TABLE `tipo_configuracion` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_amigo` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL UNIQUE,
  `telefono` varchar(255) NOT NULL,
  `direccion` longtext DEFAULT NULL,
  `clave` varchar(255) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `intereses` longtext DEFAULT NULL,
  `tipo_configuracion_id` char(30) NOT NULL DEFAULT 1,
  `comunidad_educativa` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `tipoConfiguracion` FOREIGN KEY (`tipo_configuracion_id`) REFERENCES `tipo_configuracion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `subcategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `subcategoriaCategoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `libro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isbn` varchar(255) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `editorial` varchar(255) DEFAULT NULL,
  `puntuacion` int(11) DEFAULT NULL,
  `cantidad_votos` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `lista_deseo` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `libroCategoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_modalidad` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_condicion` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `libro_usuario` (
  `usuario_id` int(11) NOT NULL,
  `libro_id` int(11) NOT NULL,
  `tipo_modalidad_id` char(30) NOT NULL,
  `tipo_condicion_id` char(30) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `precio` REAL DEFAULT 0.0,
  PRIMARY KEY (`usuario_id`,`libro_id`),
  CONSTRAINT `usuarioLibro` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `libroUsuario` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modalidadLibro` FOREIGN KEY (`tipo_modalidad_id`) REFERENCES `tipo_modalidad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `condicionLibro` FOREIGN KEY (`tipo_condicion_id`) REFERENCES `tipo_condicion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `SubcategoriaLibro` FOREIGN KEY (`subcategoria_id`) REFERENCES `subcategoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `autor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `libro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `autorLibro` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `lista_deseos` (
  `libro_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`usuario_id`,`libro_id`),
  CONSTRAINT `deseoUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `deseoLibo` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `grupo_lectura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `usuario_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `grupoLectura` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `grupo_lectura_usuario` (
  `grupo_lectura_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `tipo_amigo_id` char(30) NOT NULL, 
  PRIMARY KEY (`grupo_lectura_id`,`usuario_id`),
  CONSTRAINT `usuarioGrupo` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarioLectura` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
   CONSTRAINT `usuarioGrupoLecturaTipo` FOREIGN KEY (`tipo_amigo_id`) REFERENCES `tipo_amigo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `comentario_grupo_lectura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` longtext NOT NULL,
  `fecha_creacion` date NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `grupo_lectura_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `comentarioGrupo` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `comentarioUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `estado_solicitud` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `tipo_notificacion` (
  `id` char(30) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `notificacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_id` int(11) NOT NULL ,
  `descripcion` varchar(255) NOT NULL ,
  `tipo_notificacion_id` char(30) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `libro_id` int(11) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `grupo_lectura_id` int(11) DEFAULT NULL,
  `amigo_id` int (11) DEFAULT  NULL,
  `notificacion_vista` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `notificacionUsuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `estadoNotificacion` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LibroNotificacion` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GrupoLecturaNotificacion` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `notificacionTipo` FOREIGN KEY (`tipo_notificacion_id`) REFERENCES `tipo_notificacion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
  ) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `solicitud` (
  `usuario_solicitante_id` int(11) NOT NULL,
  `usuario_dueno_id` int(11) NOT NULL ,
  `libro_id` int(11) NOT NULL,
  `fecha_solicitud` date NOT NULL,
  `tipo_modalidad_id` char(30) NOT NULL,
  `estado_solicitud_id` char(30) NOT NULL,
  PRIMARY KEY (`usuario_solicitante_id`,`usuario_dueno_id`,`libro_id`),
  CONSTRAINT `solicitudUsuario_idx` FOREIGN KEY (`usuario_solicitante_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudLibro_idx` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudTipo_idx` FOREIGN KEY (`tipo_modalidad_id`) REFERENCES `tipo_modalidad` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `solicitudEstado_idx` FOREIGN KEY (`estado_solicitud_id`) REFERENCES `estado_solicitud` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `tipo_noticia` (
  `id` char(30) NOT NULL ,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `usuario_amigo` (
  `usuario_id` int(11) NOT NULL,
  `amigo_id` int(11) NOT NULL,
  `tipo_amigo_id` char(30) NOT NULL,
  PRIMARY KEY (`usuario_id`,`amigo_id`),
  CONSTRAINT `usuarioAmigo` FOREIGN KEY (`amigo_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `usuarioAmigoTipo` FOREIGN KEY (`tipo_amigo_id`) REFERENCES `tipo_amigo` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `noticia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_creacion` date DEFAULT NULL,
  `usuario_id` int(11) NOT NULL,
  `descripcion` varchar(255),
  `libro_id` int(11) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `grupo_lectura_id` int(11) DEFAULT NULL,
  `tipo_noticia_id` char(30) NOT NULL,
  `amigo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `UsuarioNoticia` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `estadoNoticia` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `LibroNoticia` FOREIGN KEY (`libro_id`) REFERENCES `libro` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `GrupoLecturaNoticia` FOREIGN KEY (`grupo_lectura_id`) REFERENCES `grupo_lectura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tipoNoticiaUsuario` FOREIGN KEY (`tipo_noticia_id`) REFERENCES `tipo_noticia` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


INSERT INTO `tipo_configuracion` VALUES ('NOT','Solo por la aplicación'),
('COR','Solo al correo'),
('NOTyCOR','Por la aplicación y el correo');

INSERT INTO `tipo_modalidad` VALUES ('INT','Intercambio'),
('VEN','Venta'),
('DON','Donacion');

INSERT INTO `tipo_condicion` VALUES ('NUEV','Como nuevo'),
('BUE','Buena'),
('MED','Intermedia'),
('MAL','Mala');

INSERT INTO `tipo_noticia` VALUES ('AMIG','es amigo de '),
('EST','publico un estado'),
('ENT','concreto exitosamente una negociación de un libro '),
('LIB','agrego el libro:'),
('GRUP','creo el grupo de lectura'),
('PERF','modifico su perfil'),
('EDITL','edito la informacion del libro: ');


INSERT INTO `tipo_amigo` VALUES ('ACEP','Acepto solicitud'),
('RECH','Rechazo solicitud'),
('NOR','No ha respondido');

INSERT INTO `tipo_notificacion` VALUES ('AMIGO','un amigo acepto la solicitud de amistad'),
  ('A-RECHAZADA','invitación de amistad rechazada'),
  ('COMENTARIO-L','comentaron un grupo de lectura'),
  ('P-ACEPTADA','aceptaron peticion de libro'),
  ('P-RECHAZADA','rechazaron peticion de libro'),
  ('SOLICITUDL','solicitaron un libro'),
  ('COMENTARIO-E','comentaron tu estado'),
  ('INVT-L','invitación a grupo de lectura'),
  ('LIST-DES','libro deseado disponible'),
  ('INVT-A','invitación de amistad recibida'),
  ('ELI-G', 'elimino el grupo de lectura'),
  ('ELI-G-M', 'te elimino del grupo de lectura'),
  ('G-RECHAZADA', 'rechazo la invitación al grupo de lectura'),
  ('G-EDITO','edito el grupo de lectura'),
  ('G-ACEPTADA','acepto la invitación al grupo de lectura');


INSERT INTO `estado_solicitud` VALUES ('REALIZADA','Realizada exitosamente'),
('ENVIADA','Solicitud enviada y no aceptada aun'),
('N-REALIZADA','Realizada pero no fue concluida exitosamente'),
('ACEPTADA','Solicitud aceptada esperando envio');



INSERT INTO `categoria` VALUES (1,'Escolar'),
(2,'Univesitario');



INSERT INTO `subcategoria` (`id`, `descripcion`, `categoria_id`) VALUES
(25, 'Arte', 2),
(26, 'Lenguaje', 1),
(27, 'Literatura', 1),
(28, 'Ciencia', 1),
(29, 'Matematica', 2),
(30, 'Computación', 2),
(31, 'Biología', 2),
(32, 'Comunicación social', 2);


INSERT INTO `libro` (`id`, `isbn`, `titulo`, `ano`, `editorial`, `puntuacion`, `cantidad_votos`, `categoria_id`,`lista_deseo`) VALUES
(25, '1234567890', 'Siddharta', 1990, 'Santillana', 0, 0, 2, 0),
(26, '1234567891', 'Lectura 1ro', 9889, 'Santillana', 0, 0, 1, 0),
(27, '1234567892', 'Lectura 2do', 2001, 'Enciclopedia', 0, 0, 1, 0),
(28, '1234567893', 'Ciencias para todos', 2015, 'Santillana', 0, 0, 1, 0),
(29, '1234567894', 'Matematicas 1', 2000, 'Ucv', 0, 0, 2, 0),
(30, '1234567895', 'Matemática discretas 2', 2015, 'Ucv', 0, 0, 2, 0),
(31, '1234567896', 'Biologia molecular 2', 2013, 'Lorem ipsum', 0, 0, 2, 0),
(32, '1234567897', 'Cine 1', 2001, 'Lorem ipsum', 0, 0, 2, 0),
(33, '1234567898', 'Comunicación social 1', 2032, 'Ucv', 0, 0, 2, 0);



INSERT INTO `autor` (`id`, `nombre`, `libro_id`) VALUES
(25, 'Herman hesse', 25),
(26, 'Sherezada moubayyed', 26),
(27, 'annerys rivas', 26),
(28, 'Lorem ipsum', 27),
(29, 'Jorge moubayyed', 28),
(30, 'Andres perez', 29),
(31, 'Juan enrique', 30),
(32, 'Noymar luque', 31),
(33, 'Euduar lala', 32),
(34, 'Alfredo peñate', 33);



INSERT INTO `usuario` VALUES (1,'sherezada moubayyed','mariet.moubayyed@gmail.com','04342132','Edo. vargas, macuto','827ccb0eea8a706c4c34a16891f84e7b',0,'kaskkskss','NOT','ceapucv',NULL),(2,'Heyiner Espina','heyi@gmail.com','04342132','Edo. vargas, macuto','827ccb0eea8a706c4c34a16891f84e7b',0,'kaskkskss','NOT','ceapucv',NULL),(25,'Zeus','zeus@gmail.com','02128938393','carcas','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','NOT','ceapucv',NULL),(26,'Jorge moubayyed','jorge@gmail.com','02128272827','casa','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv',NULL),(27,'Hector navarro','hector.navarro@ciens.ucv.ve','04168461434','Caracas','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv',NULL),(28,'Esmitt ramirez','esmitt.ramirez@gmail.com','04161083339','sta rosalia','e10adc3949ba59abbe56e057f20f883e',0,'ciencias-literatura-matemática-sociales-historia-geografía-física-química-inglés-computación','COR','ceapucv',NULL),(29,'Annerys rivas','annerys.rivas@gmail.com','04123735584','','e10adc3949ba59abbe56e057f20f883e',7,'-inglés-computación','NOTyCOR','ciencias',NULL);

INSERT INTO `usuario_amigo` (`usuario_id`, `amigo_id`, `tipo_amigo_id`) VALUES
(1, 26, 'ACEP'),
(2, 26, 'ACEP');

INSERT INTO `libro_usuario` VALUES (1,25,'DON','BUE',2,25,0),(1,26,'INT','MED',1,26,0),(1,27,'VEN','BUE',1,27,125.68),(1,28,'DON','BUE',1,28,0),(1,29,'INT','MED',2,29,0),(1,30,'DON','BUE',2,30,0),(1,31,'VEN','MED',2,25,346.78),(1,32,'VEN','BUE',2,25,599.99),(1,33,'INT','MAL',2,25,0),(29,25,'VEN','BUE',1,26,100);


INSERT INTO `noticia` (`id`, `fecha_creacion`, `usuario_id`, `descripcion`, `libro_id`, `estado_id`, `grupo_lectura_id`, `tipo_noticia_id`, `amigo_id`) VALUES (25, '2015-08-01', 2, 'es amigo de ', NULL, NULL, NULL, 'AMIG', 26), (26, '2015-08-01', 1, 'es amigo de ', NULL, NULL, NULL, 'AMIG', 26);

INSERT INTO `notificacion` (`id`, `usuario_id`, `descripcion`, `tipo_notificacion_id`, `fecha_creacion`, `libro_id`, `estado_id`, `grupo_lectura_id`, `amigo_id`, `notificacion_vista`) VALUES
(25, 1, 'El usuario Jorge moubayyed te agregó como amigo', 'INVT-A', '2015-08-01', NULL, NULL, NULL, 26, 1),
(26, 2, 'El usuario Jorge moubayyed te agregó como amigo', 'INVT-A', '2015-08-01', NULL, NULL, NULL, 26, 1),
(27, 26, 'El usuario Heyiner Espina acepto tu solicitud de amistad', 'AMIGO', '2015-08-01', NULL, NULL, NULL, 2, 0),
(28, 26, 'El usuario sherezada moubayyed acepto tu solicitud de amistad', 'AMIGO', '2015-08-01', NULL, NULL, NULL, 1, 0);
